#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 全局常量

GLOBAL_EMPTY_STR    = ''
GLOBAL_SPACE_CHAR   = ' '


GLOBAL_WORK_DIR     = r"D:\share\test"
GLOBAL_FILE_NAME    = r"%s" % __import__('time').strftime("%Y%m%d")
GLOBAL_TXT_FILE     = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".txt")
GLOBAL_LOG_FILE     = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".log")
GLOBAL_ERROR_FILE   = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".error")
GLOBAL_CSV_FILE     = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".csv")
GLOBAL_XLS_FILE     = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".xls")
GLOBAL_XLSX_FILE    = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".xlsx")
GLOBAL_DOCX_FILE    = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".docx")

GLOBAL_MP3_FILE     = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".mp3")
GLOBAL_MP4_FILE     = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".mp4")

GLOBAL_JPG_FILE     = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".jpg")
GLOBAL_PNG_FILE     = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".png")
GLOBAL_GIF_FILE     = __import__('os').path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".gif")

