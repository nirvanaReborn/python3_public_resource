#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 进行选择

import os


def is_zip_file(filename):
    ext = os.path.splitext(filename)[1]
    with open(filename, 'rb') as f:
        header = f.read(2)

    if ext == '.zip' and header == b'PK':
        print('ZIP file')
        return True
    elif ext == '.gz' and header == b'\x1f\x8b':
        print('GZIP file')
        return True
    return False


def trade_time_check():
    ''' 判断当前时间是否为想交易的时间9：35-15：00，
        是返回1  不是为0 全局变量'''
    import datetime
    now_time = datetime.datetime.now()
    start_time = datetime.datetime(now_time.year, now_time.month, now_time.day, 9, 35, 0)
    end_time = datetime.datetime(now_time.year, now_time.month, now_time.day, 15, 0, 0)
    if now_time.__ge__(start_time) and now_time.__le__(end_time):
        return True
    else:
        return False


def make_choice(dict_choice, function_name=None, args=()):
    if function_name == None:
        function_name = exec

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        function_name(dict_choice[choice], *args)
    elif choice == "a":
        for choice in dict_choice:
            function_name(dict_choice[choice], *args)
    else:
        print("输入有误，请重输！", choice)


def main():
    # make_choice(dict_choice)
    print(trade_time_check())


if __name__ == "__main__":
    main()
