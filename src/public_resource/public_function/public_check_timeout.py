#!/usr/bin/env python3
# -*- coding:utf-8 -*-
from concurrent.futures import ThreadPoolExecutor
import threading
import time
# import logging


def timeout_handler(future):
    if not future.done():
        future.cancel()
        raise TimeoutError()


def check_timeout(func):
    """检查函数执行是否超时。未超时：返回函数结果；超时：返回None。"""

    def wrapper(*args, **kwargs):
        executor = ThreadPoolExecutor(max_workers=1)
        future = executor.submit(func, *args, **kwargs)
        timer = threading.Timer(5, timeout_handler, args=(future,))
        timer.start()
        try:
            info = future.result(timeout=5)
            timer.cancel()
            return info
        except TimeoutError:
            timer.cancel()
            # logging.error("函数%s执行超时" % func.__name__)
            print("函数%s执行超时" % func.__name__)
            return None

    return wrapper


@check_timeout
def test_check_timeout_1():
    return "函数test_check_timeout1执行未超时"


@check_timeout
def test_check_timeout_2():
    time.sleep(6)
    return "函数test_check_timeout2执行超时"


def main():
    test_check_timeout_1()
    test_check_timeout_2()


if __name__ == '__main__':
    main()
