#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://github.com/cython/cython
# https://github.com/lostaway/EasonCodeShare/blob/master/cython_tutorials/hello_world/Makefile

# http://docs.cython.org/en/latest/src/tutorial/cython_tutorial.html

# https://blog.csdn.net/qtlyx/article/details/80614608
# 将 .pyx/.py 编译为 .c 文件，再将 .c 文件编译为 .so(Unix)或 .pyd(Windows)

# https://www.cnblogs.com/freeweb/p/6548208.html
# Cython的简单使用

# https://www.jianshu.com/p/69dc6a53b0a9
# https://www.jianshu.com/p/0dffe9aeba29
# <cython>学习

# https://blog.csdn.net/feijiges/article/details/77932382
# Cython的用法以及填坑姿势

# https://blog.csdn.net/fireflychh/article/details/80162981
# distutils 是 python 标准库的一部分，这个库的目的是为开发者提供一种方便的打包方式，
# 同时为使用者提供方便的安装方式。
# 当我们开发了自己的模块之后，使用distutils的setup.py打包。
# setuptools是distutils的增强版。(Python3.10版本中distutils 被弃用)
from setuptools import setup, Extension

# cythonize()是Cython提供将Python代码转换成C代码的API
from Cython.Build import cythonize

# 注意，在setup脚本中的路径必须以Unix形式来书写，也就是由”/”分割的。
# 该模块会在使用这些路径之前，将这种表示方法转换为适合当前平台的格式。
setup(
    name='publicResource',  # 对外我们模块的名字
    version='1.0',  # 版本号
    description='支持日常工作处理的公共库',  # 描述
    author='zt',  # 作者
    author_email='test@163.com',
    py_modules=['public_class.public_class_ASCIIFile', 'public_constant.public_resource_constant'],  # 要发布的模块
    # 使用build_dir参数指明C代码输出目录。
    ext_modules=cythonize("public_function_summary.py", build_dir="complier_build"),
)

# 模块的本地发布
# python setup.py sdist

# 本地安装模块
# python setup.py install

# 上传并远程发布
# python setup.py sdist upload

# 将Python代码转换成C代码
# 执行编译命令： python setup.py build_ext -inplace
