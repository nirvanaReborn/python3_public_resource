#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 获取时间

import logging
# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
import time
import datetime



class MyTime():
    # 初始化函数，类似于构造函数
    def __init__(self):
        super(MyTime, self).__init__()

    def timer(self, function):
        import timeit
        logging.debug('*' * 40 + "\n执行脚本总用时 %s 秒" % timeit.timeit(function, number=1))

    def timer_2(self, function):
        import time
        begintime = time.time()
        # 执行函数
        function()
        endtime = time.time()
        usetime =  endtime - begintime
        logging.debug('*' * 40)
        logging.debug("执行脚本总用时 %s 秒" % usetime)

    def timer_3(self):
        print(time.strftime("%Y-%m-%d %H:%M:%S"))
        print(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())))

        print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    def get_next_nano(self, time_str):
        cur_time = datetime.datetime.now()
        parse_time = datetime.datetime.strptime(time_str, "%H:%M:%S")
        new_time = cur_time.replace(hour=parse_time.hour, minute=parse_time.minute, second=parse_time.second)
        if new_time < cur_time:
            new_time += datetime.timedelta(days=1)
        return time.mktime(new_time.timetuple()) * 1e9


def main():
    MyTime().timer_3()
    print(MyTime().get_next_nano("15:00:00"))  # 1.6303932e+18
    print(MyTime().get_next_nano("15:00:01") - MyTime().get_next_nano("15:00:00"))  # 1000000000.0


if __name__ == "__main__":
    main()
