#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 数据比较、数据去重、数据查找、数据替换

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')


class Data():
    # 初始化函数，类似于构造函数
    def __init__(self):
        super(Data, self).__init__()

    def data_type_conversion(self, cell_value):
        # logging.debug(type(cell_value).__name__)
        if isinstance(cell_value, int):
            return str(cell_value)
        elif isinstance(cell_value, float):
            # return str(int(cell_value))
            return str(float(cell_value))
        elif isinstance(cell_value, str):
            return cell_value
        else:
            return cell_value

    def data_value_conversion(self, cell_value):
        if cell_value == None:
            return " "
        elif cell_value == "":
            return " "
        # elif "'" in str(cell_value):
        #     return str(cell_value).replace("'", "''")
        else:
            return cell_value

    def bool_convert_to_str(self, cell_value):
        # bool()
        if cell_value == "false":
            cell_value = "0"
        else:
            cell_value = "1"
        return cell_value

    # 数据去重
    def duplicate_removal_list1D(self, list_1D):
        set_1D = set()
        for i, item in enumerate(list_1D):
            set_1D.add(str(item).strip("\n"))

        # for i, item in enumerate(sorted(list(set_1D))):
        #     print(item)
        return sorted(list(set_1D))

    # 数据去重
    def duplicate_removal_list2D(self, list_2D, index):
        set_1D = set()
        for i, fileLine in enumerate(list_2D):
            set_1D.add(str(fileLine[index]).strip("\n"))

        # for i, item in enumerate(sorted(list(set_1D))):
        #     print(item)
        return sorted(list(set_1D))

    # 提取二位列表中的某一列作为一维列表
    def get_list1D_from_list2D(self, list_2D, index=0):
        list_1D = []
        for i, fileLine in enumerate(list_2D):
            list_1D.append(fileLine[index])
        return list_1D

    # 统计使用频率(statistical usage frequency)
    def count_usage_frequency_by_set(self, list_1D):
        list_count = []
        myset = set(list_1D)
        for item in myset:
            list_count.append((item, list_1D.count(item)))
        list_count.sort(key=lambda k: k[1], reverse=True)
        return list_count

    # 统计使用频率
    def count_usage_frequency_by_dict(self, object):
        # object可以是list或字符串
        dict_count = {}
        for item in object:
            dict_count[item] = dict_count.get(item, 0) + 1
        list_count = sorted(dict_count.items(), key=lambda d: d[1], reverse=True)
        # dict(collections.Counter(list_1D))
        return list_count

    # 获取重复数据
    def get_repeat_data(self, object):
        list_repeat_data = []
        list_count = self.count_usage_frequency_by_dict(object)
        for i, fileLine in enumerate(list_count):
            if fileLine[1] > 1:
                list_repeat_data.append(fileLine[0])
        return list_repeat_data

    def is_chinese(self, uchar):
        """判断一个unicode是否是汉字"""
        if uchar >= u'\u4e00' and uchar <= u'\u9fa5':
            return True
        else:
            return False

    def is_number(self, string):
        try:
            float(string)
            return True
        except ValueError:
            pass

        try:
            import unicodedata
            unicodedata.numeric(string)
            return True
        except (TypeError, ValueError):
            pass

        return False

    def is_exist_keyword(self, target, source):
        if isinstance(target, str):
            if target in source:
                return True
        elif isinstance(target, list):
            for item in target:
                if item in source:
                    return True
        elif isinstance(target, dict):
            for key in target:
                if target[key] in source:
                    return True
        return False

    def is_equal_keyword(self, source, substr):
        if isinstance(source, str):
            if source == substr:
                return True
        elif isinstance(source, list):
            for item in source:
                if item == substr:
                    return True
        elif isinstance(source, dict):
            for key in source:
                if source[key] == substr:
                    return True
        return False

    def search_match_key_word(self, target, source):
        str_result = ""
        if isinstance(target, str):
            if target in source:
                str_result = target
        elif isinstance(target, list):
            for item in target:
                if item in source:
                    str_result = str(item)
        elif isinstance(target, dict):
            for key in target:
                if target[key] in source:
                    str_result = str(key)
        return str_result

    def replace_keyword(self, source, keyword, list_new_word):
        flag = ""
        if isinstance(keyword, str):
            if keyword in source:
                flag = source.replace(keyword, list_new_word)
        elif isinstance(keyword, list):
            for i in range(len(keyword)):
                if keyword[i] in source:
                    flag = source.replace(keyword[i], list_new_word[i])
        elif isinstance(keyword, dict):
            for key in keyword:
                if keyword[key] in source:
                    flag = source.replace(keyword[key], list_new_word[key])
        return flag

    def number_range_comparison(self, list_range, cell_value):
        if list_range[0] > list_range[1]:
            print("数值区间用法错误", list_range)
            return False

        if list_range[2] == "in":
            if list_range[0] < cell_value < list_range[1]:
                return True
        elif list_range[2] == "out":
            if cell_value < list_range[0] or cell_value > list_range[1]:
                return True
        elif list_range[2] == "no":
            return True
        else:
            print("数值比较用法错误", list_range[2])
            return False

    def get_row_col(self, list_2D):
        max_row = len(list_2D)
        max_column = len(list_2D[0])
        return max_row, max_column

    # 反转一个整数，例如-123 --> -321
    def reverse(self, x):
        if -10 < x < 10:
            return x
        str_x = str(x)
        if str_x[0] != "-":
            str_x = str_x[::-1]
            x = int(str_x)
        else:
            str_x = str_x[1:][::-1]
            x = int(str_x)
            x = -x
        return x if -2147483648 < x < 2147483647 else 0


def main():
    data = Data()
    list_1D = [66.25, 333, 333, 1, 1234.5]
    # list_count = count_usage_frequency_by_dict(list_1D)
    # for item in list_count:
    #     print(item)
    list_repeat_data = data.get_repeat_data(list_1D)
    for item in list_repeat_data:
        print(item)


if __name__ == "__main__":
    main()
