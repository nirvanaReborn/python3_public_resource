#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 解压缩文件

import os
from public_class.public_class_FileInfo import FileInfo

format = ['rar', 'zip', '7z', 'ace', 'arj', 'bz2', 'cab', 'gz', 'iso', 'jar', 'lzh', 'tar', 'uue', 'z']


class Decompression(FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name, opt='mkdir'):
        super(Decompression, self).__init__(file_name)
        if os.path.isdir(file_name):
            self.source_dir = os.path.abspath(file_name)
        else:
            self.source_file = os.path.abspath(file_name)
        self.opt = opt

    def unzipfile(self):
        if os.path.isfile(self.source_file) and (os.path.splitext(self.source_file)[1][1:].lower() in format) == True:
            if self.opt == 'mkdir':
                cmd = 'winrar x -ibck "' + self.source_file + '"' + ' "' + os.path.splitext(self.source_file)[0] + '"\\'
                os.system(cmd)
            elif self.opt == 'direct':
                cmd = 'winrar x -ibck "' + self.source_file + '"'
                os.system(cmd)

    def unzipfile_dir(self):
        os.chdir(self.source_dir)
        for file in os.listdir('.'):
            if os.path.isfile(file) and (os.path.splitext(file)[1][1:].lower() in format) == True:
                if self.opt == 'mkdir':
                    cmd = 'winrar x -ibck "' + file + '"' + ' "' + os.path.splitext(file)[0] + '"\\'
                    os.system(cmd)
                elif self.opt == 'direct':
                    cmd = 'winrar x -ibck "' + file + '"'
                    os.system(cmd)

    # def unzip(self):
    #     suffix = self.get_file_suffix()
    #     if ".zip" == suffix:
    #         self.unzipfile()
    #     else:
    #         print("不支持解压缩该文件:", self.source_file)


def main():
    pass


if __name__ == "__main__":
    main()
