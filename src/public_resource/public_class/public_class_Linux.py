#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.cnblogs.com/haigege/p/5513759.html
# 使用paramiko执行远程linux主机命令
# ssh: 安全外壳协议
# paramiko是python的SSH库，可用来连接远程linux主机，然后执行linux命令或者通过SFTP传输文件。

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
logging.getLogger("paramiko").setLevel(logging.WARNING)
import paramiko
# import fabric # Fabric在paramiko的基础上做了更高一层的封装，操作起来更加简单。
import os
import re
import time
from public_class.public_class_GetConfigInfo import GetConfigInfo
from public_constant.public_resource_constant import GLOBAL_WORK_DIR


# 定义一个类，表示一台远端linux主机
class Linux():
    # 通过IP, 用户名，密码，超时时间初始化一个远程Linux主机
    def __init__(self, dict_login_info):
        super(Linux, self).__init__()
        self.host = dict_login_info["ip"]
        self.username = dict_login_info["user"]
        self.password = dict_login_info["password"]
        self.timeout = int(dict_login_info["timeout"])
        self.port = int(dict_login_info["port"])
        # transport和chanel
        self.t = ''
        self.chan = ''
        # 链接失败的重试次数
        self.try_times = 3

        # 调用该方法连接远程主机
        while True:
            # 连接过程中可能会抛出异常，比如网络不通、链接超时
            try:
                self.t = paramiko.Transport(self.host, self.port)
                self.t.connect(username=self.username, password=self.password)
                self.sftp = paramiko.SFTPClient.from_transport(self.t)
                self.chan = self.t.open_session()
                self.chan.settimeout(self.timeout)
                self.chan.get_pty()
                self.chan.invoke_shell()
                # 如果没有抛出异常说明连接成功，直接返回
                print(('*' * 20 + '连接{0}成功' + '*' * 20).format(self.host))
                # 接收到的网络数据解码为str
                # print(self.chan.recv(65535).decode('utf-8'))
                return
            # 这里不对可能的异常如socket.error, socket.timeout细化，直接一网打尽
            except Exception as e:
                if self.try_times != 0:
                    print(u'连接%s失败，进行重试' % self.host)
                    self.try_times -= 1
                else:
                    print(u'重试3次失败，结束程序')
                    exit(1)

    # 断开连接
    def close(self):
        self.chan.close()
        self.t.close()
        print(('*' * 20 + '退出{0}成功' + '*' * 20).format(self.host))

    def __exit__(self):
        self.chan.close()
        self.t.close()

    # 发送要执行的命令
    def send(self, cmd):
        # print(cmd)
        cmd += '\r'
        # 通过命令执行提示符来判断命令是否执行完成
        p = re.compile(r'$')

        result = ''
        ret = ''
        # 发送要执行的命令
        self.chan.send(cmd)
        # 回显很长的命令可能执行较久，通过循环分批次取回回显
        while True:
            time.sleep(2)
            try:
                ret = self.chan.recv(65535).decode('gb2312')
                result += ret
            except Exception as e:
                print(e)

            if p.search(ret):
                # print(result)
                return result

    def sftp_upload(self, local, remote):
        try:
            if os.path.isdir(local):  # 判断本地参数是目录还是文件
                for file in os.listdir(local):  # 遍历本地目录
                    self.sftp.put(os.path.join(local + file), os.path.join(remote + file))  # 上传目录中的文件
                    print("上传成功：", os.path.join(local + file), "---->", os.path.join(remote + file))
            else:
                self.sftp.put(local, remote)  # 上传文件
                print("上传成功：", local, "---->", remote)
        except Exception as e:
            print('upload exception:', e)

    def sftp_download(self, local, remote):
        try:
            if os.path.isdir(local):  # 判断本地参数是目录还是文件
                for file in self.sftp.listdir(remote):  # 遍历远程目录
                    self.sftp.get(os.path.join(remote + file), os.path.join(local + file))  # 下载目录中文件
                    print("下载成功：", os.path.join(remote + file), "---->", os.path.join(local + file))
            else:
                self.sftp.get(remote, local)  # 下载文件
                print("下载成功：", remote, "---->", local)
        except Exception as e:
            print('download exception:', e)

    # 通过发送linux命令到远程服务器，然后将结果重定向到文件中，最后将结果文件下载到本地
    def remote_access_by_shell_cmd(self, dest_file, shell_cmd):
        if self.username != "root":
            remote_file = r"/home/" + self.username + r"/output.log"
        else:
            remote_file = r"/root/output.log"
        linux_cmd = shell_cmd + " > " + remote_file
        # print(linux_cmd)
        self.send(linux_cmd)
        self.sftp_download(dest_file, remote_file)
        self.send('rm ' + remote_file)

    def remote_access_by_shell_cmd_array(self, dest_file, list_shell_cmd):
        if self.username != "root":
            remote_file = r"/home/" + self.username + r"/output.log"
        else:
            remote_file = r"/root/output.log"

        for shell_cmd in list_shell_cmd:
            linux_cmd = shell_cmd + " >> " + remote_file
            print(linux_cmd)
            self.send(linux_cmd)
        self.sftp_download(dest_file, remote_file)
        self.send('rm ' + remote_file)

    # 通过发送linux脚本到远程服务器，然后将结果重定向到文件中，最后将结果文件下载到本地
    def remote_access_by_shell_script(self, dest_file, shell_file, cmd_args=""):
        if not os.path.exists(shell_file):
            print("脚本不存在:", shell_file)
            return

        if self.username != "root":
            remote_file = r"/home/" + self.username + r"/output.log"
            shell_script = r"/home/" + self.username + "/" + os.path.basename(shell_file)
        else:
            remote_file = r"/root/output.log"
            shell_script = r"/root/" + os.path.basename(shell_file)
        self.sftp_upload(shell_file, shell_script)
        linux_cmd = "sh " + shell_file + " " + cmd_args + " > " + remote_file
        # print(linux_cmd)
        self.send(linux_cmd)
        self.sftp_download(dest_file, remote_file)
        self.send('rm ' + shell_file)
        self.send('rm ' + remote_file)


def main():
    # login_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xlsx")
    dest_file = os.path.join(GLOBAL_WORK_DIR, r"test.txt")
    # dict_login_info = GetConfigInfo(login_file).get_list_config()[0]
    # # print(dict_login_info)

    dict_login_info = {
        "ip": "10.20.23.16",
        "user": "ms",
        "password": "hundsun",
        "timeout": 60,
        "port": 22,
    }
    ssh = Linux(dict_login_info)
    ssh.remote_access_by_shell_cmd(dest_file, 'ls -l')
    ssh.remote_access_by_shell_script(dest_file, r'get_system_info.sh', "1")
    ssh.close()


if __name__ == "__main__":
    main()
