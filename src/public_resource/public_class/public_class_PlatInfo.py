#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
os提供操作系统的接口，常用的有文件系统相关和进程相关

sys提供python解释器系统的通用配置和函数，影响着解释器的行为

platform提供平台相关的信息
'''

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
logging.getLogger("selenium").setLevel(logging.WARNING)
from selenium import webdriver
import struct
import os
import sys
import platform


class PlatInfo():
    # 初始化函数，类似于构造函数
    def __init__(self):
        super(PlatInfo, self).__init__()

    def get_exe_path(self, args=()):
        exe_path = None
        sys_str = platform.system()
        if sys_str == 'Windows':
            list_path = os.environ["PATH"].split(";")
            for env_path in list_path:
                # print(env_path)
                if args[0] in env_path:
                    if args[1] in env_path:
                        exe_path = env_path
                        break
                    else:
                        exe_path = os.path.join(env_path, args[1])
                        # print(exe_path)
                        if os.path.exists(exe_path):
                            break
                        else:
                            continue
                else:
                    exe_path = None
        else:
            print("未识别操作系统:", sys_str)

        if exe_path != None and os.path.exists(exe_path):
            pass
        else:
            print("路径不存在：", args)

        return exe_path

    # ------------------------------------------------------------
    # 解释器
    def get_python_exe_path(self):
        sys_str = platform.system()
        if sys_str == 'Linux':
            interpreter = r"/usr/bin/python3"
        elif sys_str == 'Windows':
            args = ("Anaconda3", "python.exe")
            interpreter = self.get_exe_path(args)
        else:
            print("未识别操作系统:", sys_str)
            interpreter = None

        return interpreter

    def get_python_pip_path(self):
        sys_str = platform.system()
        if sys_str == 'Linux':
            exe_path = r"/usr/bin/pip3"
        elif sys_str == 'Windows':
            args = ("Scripts", "pip.exe")
            exe_path = self.get_exe_path(args)
        else:
            print("未识别操作系统:", sys_str)
            exe_path = None

        return exe_path

    def get_python_2to3_path(self):
        args = ("Scripts", "2to3.exe")
        return self.get_exe_path(args)

    def get_python_pyinstaller_path(self):
        args = ("Scripts", "pyinstaller.exe")
        return self.get_exe_path(args)

    # ------------------------------------------------------------
    # http://blog.csdn.net/chinaren0001/article/details/8549259
    # Python获取当前系统用户名
    # 这个模块无论在linux还是在windows上都可以使用。
    def get_user_name(self):
        import getpass
        return getpass.getuser()

    #
    # # 使用os.getenviron,这一做法有点不安全，是因为它跟环境变量设置有关。
    # def test_1():
    #     import os
    #     user = os.environ['USERNAME']
    #     print(user)
    #
    # # 如果你使用的是linux系统，获取方式还有：
    # def test_2():
    #     import os
    #     import pwd
    #     user = pwd.getpwuid(os.getuid())[0]
    #     print(user)

    def get_system_platform(self):
        """
        Get a string that specifies the platform more specific than sys.platform does.
        The result can be:
        linux32, linux64, win32, win64, osx32, osx64.
        Other platforms may be added in the future.
        """
        if sys.platform.startswith('linux'):
            plat = 'linux%i'
        elif sys.platform.startswith('win'):
            plat = 'win%i'
        elif sys.platform.startswith('darwin'):
            plat = 'osx%i'
        else:  # pragma: no cover
            return None
        return plat % (struct.calcsize('P') * 8)  # 32 or 64 bits

    # 根据操作系统指明被遍历的目录
    def get_filepath_by_system_plat(self):
        # dict_system_plat = {
        #     'Windows': r"C:\Program Files",
        #     'Linux'  : r"/boot",
        #     'Darwin' : r"/boot",
        # }
        if platform.system() == 'Windows':
            filepath = r"C:\Windows\Resources"
        elif platform.system() == 'Linux':
            filepath = r"/boot"
        elif platform.system() == 'Darwin':
            filepath = r"/boot"
        else:
            filepath = None
            print("未识别操作系统")
        return filepath

    def get_browser_chrome(self):
        chromedriver = r"C:\Program Files (x86)\Google\Chrome\Application\chromedriver.exe"
        os.environ["webdriver.chrome.driver"] = chromedriver
        # 加启动配置
        option = webdriver.ChromeOptions()
        # 让chrome浏览器不出现‘Chrome正在受到自动软件的控制’的提示语
        option.add_argument('disable-infobars')
        # 启动浏览器的时候不想看浏览器运行，那就加载浏览器的静默模式，让它在后台偷偷运行
        # option.add_argument('headless')
        browser = webdriver.Chrome(chromedriver, chrome_options=option)
        # print(type(browser)) # <class 'selenium.webdriver.chrome.webdriver.WebDriver'>
        return browser


def main():
    print(PlatInfo().get_python_exe_path())
    print(PlatInfo().get_python_pip_path())
    print(PlatInfo().get_python_2to3_path())
    print(PlatInfo().get_python_pyinstaller_path())

    print(PlatInfo().get_user_name())
    print(PlatInfo().get_system_platform())
    print(PlatInfo().get_filepath_by_system_plat())

    browser = PlatInfo().get_browser_chrome()
    browser.get('http://inventwithpython.com')


if __name__ == "__main__":
    main()
