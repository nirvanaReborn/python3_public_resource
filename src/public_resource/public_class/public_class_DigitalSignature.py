#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 数字签名
'''
# http://blog.csdn.net/sauphy/article/details/49916471
# python之生成文件/目录的md5值
# import md5 # 在python3中已经废弃了md5和sha模块
'''

import logging
# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
import hashlib
import os


class DigitalSignature():
    # 初始化函数，类似于构造函数
    def __init__(self):
        super(DigitalSignature, self).__init__()


    ########################################
    def get_Md5_Of_String(self, string):
        md1 = hashlib.md5()
        md1.update(string.encode())
        return md1.hexdigest()


    ########################################
    def get_Md5_Of_File(self, filename):
        if not os.path.isfile(filename):
            return
        myhash = hashlib.md5()
        f = open(filename, 'rb')
        while True:
            b = f.read(1024)
            if not b:
                break
            myhash.update(b)
        f.close()
        return myhash.hexdigest()


    ########################################
    def get_Md5_Of_Folder(self, dir):
        MD5File = "tmp.md5"
        outfile = open(MD5File, 'w')
        for root, subdirs, files in os.walk(dir):
            for file in files:
                filefullpath = os.path.join(root, file)
                filerelpath = os.path.relpath(filefullpath, dir)
                md5 = self.get_Md5_Of_File(filefullpath)
                outfile.write(md5)
        outfile.close()
        return self.get_Md5_Of_File(MD5File)



def main():
    obj = DigitalSignature()
    print(obj.get_Md5_Of_String("abc"))
    print("#" * 50)
    print(obj.get_Md5_Of_File("../public_constant/public_resource_constant.py"))
    print("#" * 50)
    print(obj.get_Md5_Of_Folder(os.getcwd()))


if __name__ == "__main__":
    main()
