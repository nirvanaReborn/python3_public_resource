#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 处理json文档
'''
json.dumps(): 对数据进行编码,将 Python 对象编码成 JSON 字符串。
json.loads(): 对数据进行解码,将已编码的 JSON 字符串解码为 Python 对象。

1、json.dumps()和json.loads()是json格式处理函数（可以这么理解，json是字符串），针对内存对象：
　　(1)json.dumps()函数是将一个Python数据类型列表进行json格式的编码（可以这么理解，json.dumps()函数是将字典转化为字符串）
　　(2)json.loads()函数是将json格式数据转换为字典（可以这么理解，json.loads()函数是将字符串转化为字典）
2、json.dump()和json.load()针对文件句柄，主要用来读写json文件函数。
'''
import os
import json
from public_class.public_class_FileInfo import FileInfo
from public_constant.public_resource_constant import GLOBAL_WORK_DIR


class JSON(FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name):
        super(JSON, self).__init__(file_name)
        self.source_file = os.path.abspath(file_name)

    def get_text(self):
        try:
            # 设置以utf-8解码模式读取文件，encoding参数必须设置，
            # 否则默认以gbk模式读取文件，当文件中包含中文时，会报错
            with open(self.source_file, 'r', encoding='utf-8') as fd:
                file_text = json.load(fd)
        except Exception as e:
            print("配置文件解析失败:", self.source_file, e)
            file_text = {}
        return file_text

    def set_text(self, json_info):
        # json.dump()函数的使用，将json信息写进文件
        fd = open(self.source_file, 'w', encoding='utf-8')
        json.dump(json_info, fd)


def main():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"json_demo.json")
    JSON(source_file).get_text()

    dest_file = os.path.join(GLOBAL_WORK_DIR, r"1.json")
    json_info = "{'age': '12'}"
    JSON(dest_file).set_text(json_info)


if __name__ == "__main__":
    main()
