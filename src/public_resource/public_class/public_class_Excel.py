#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 处理excel文件

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')

import xlrd
import xlwt
import colorama
import os
import openpyxl
import pandas

try:
    from openpyxl.cell import get_column_letter
except ImportError:
    from openpyxl.utils import get_column_letter
from public_class.public_class_FileInfo import FileInfo
from public_class.public_class_ASCIIFile import ASCIIFile
from public_class.public_class_Data import Data
from public_constant.public_resource_constant import GLOBAL_XLSX_FILE, GLOBAL_WORK_DIR


class Excel_Pandas(FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name=GLOBAL_XLSX_FILE):
        super(Excel_Pandas, self).__init__(file_name)
        self.source_file = os.path.abspath(file_name)

    def create_excel_pandas(self, dict_sheet_name, dict_head, list3D_sheet, append=False, index=False, header_flag=True):
        if list3D_sheet != None and len(list3D_sheet) > 0:
            # if_sheet_exists = replace   # 写入Sheet页方式为覆盖
            # if_sheet_exists = 'overlay' # 写入Sheet页方式为追加
            with pandas.ExcelWriter(self.source_file, engine='openpyxl', mode='a', if_sheet_exists='replace') as writer:
                if pandas.__version__ < '1.5.0':
                    if os.path.exists(self.source_file):
                        wb = openpyxl.load_workbook(writer.path)
                        writer.book = wb
                    else:
                        wb = openpyxl.Workbook()
                else:
                    wb = writer.book
                    worksheets = writer.sheets
                    # worksheet1 = worksheets['Sheet1']

                for i, key in enumerate(list(dict_sheet_name.keys())):
                    if pandas.__version__ < '1.5.0':
                        if append:
                            pass
                        else:
                            if key in wb.sheetnames:
                                wb.remove(wb[key])

                    df = pandas.DataFrame(list3D_sheet[i], columns=list(dict_head[key]))
                    if index:
                        df.set_index([df.columns.tolist()[0]], inplace=True)
                        df = df.sort_index(axis=0, ascending=True)
                    df.to_excel(writer, sheet_name=dict_sheet_name[key], index=index, header=header_flag)

    def create_excel_pandas_by_dict(self, dict_sheet, append=False, index=False, header_flag=True):
        dict_head = {}
        dict_sheet_name = {}
        list3D_sheet = []

        if dict_sheet:
            for key in dict_sheet:
                list_2D = dict_sheet[key]
                if header_flag:
                    dict_head[key] = list_2D.pop(0)
                else:
                    dict_head[key] = "0"

                dict_sheet_name[key] = key
                list3D_sheet.append(list_2D)

            self.create_excel_pandas(dict_sheet_name, dict_head, list3D_sheet, append, index, header_flag)

    def create_excel_by_function(self, dict_source_file, str_header, function_name, arg=(), append=False, index=False, header_flag=True):
        dict_head = {}
        dict_sheet_name = {}
        list3D_sheet = []

        for key in dict_source_file:
            if arg:
                list_sheet = function_name(dict_source_file[key], arg)
            else:
                list_sheet = function_name(dict_source_file[key])

            if list_sheet != None and len(list_sheet) > 0:
                dict_head[key] = str_header.split()
                dict_sheet_name[key] = key
                list3D_sheet.append(list_sheet)

        self.create_excel_pandas(dict_sheet_name, dict_head, list3D_sheet, append, index, header_flag)


class Excel_XLSX(FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name=GLOBAL_XLSX_FILE):
        super(Excel_XLSX, self).__init__(file_name)
        self.source_file = os.path.abspath(file_name)

    # 删除excel文件中的sheet页
    def del_sheet(self, wb, list_sheet):
        # sheetnames = wb.get_sheet_names()  # 获取读文件中所有的sheet，通过名字的方式
        set_del_sheets = set(wb.sheetnames) & set(list_sheet)
        for sheet in set_del_sheets:
            # wb.remove_sheet(wb.get_sheet_by_name(sheet))
            # del wb[sheet]
            wb.remove(wb[sheet])

        return wb

    def set_column_width(self, workSheet, list_sheet):
        column_widths = []
        for row in list_sheet:
            if row:
                for i, cell in enumerate(row):
                    if len(column_widths) > i:
                        if len(str(cell)) > column_widths[i]:
                            column_widths[i] = len(str(cell))
                    else:
                        column_widths += [len(str(cell))]

        for i, column_width in enumerate(column_widths):
            workSheet.column_dimensions[get_column_letter(i + 1)].width = column_width
        return workSheet

    def create_excel_xlsx(self, dict_sheet, append=False, column_width_auto=False):
        if append == False:
            wb = openpyxl.Workbook()
        else:
            if os.path.exists(self.source_file):
                wb = openpyxl.load_workbook(self.source_file, data_only=True)
            else:
                wb = openpyxl.Workbook()

        for key, value in dict_sheet.items():
            if value != None and len(value) > 0:
                if key in wb.sheetnames:
                    # workSheet = wb.get_sheet_by_name(key)
                    workSheet = wb[key]
                else:
                    workSheet = wb.create_sheet(key)

                if column_width_auto:
                    workSheet = self.set_column_width(workSheet, value)

                for i, fileLine in enumerate(value):
                    if fileLine and len(str(fileLine)) > 0:
                        # workSheet.append(fileLine)
                        for j, item in enumerate(fileLine):
                            # logging.debug(item)
                            try:
                                workSheet.cell(row=i + 1, column=j + 1).value = str(item)
                                j += 1
                            except:
                                pass
                                # 仅仅是urllib2.unquote_plus解码是不够的，需要将特殊字符去掉
                                # openpyxl.utils.exceptions.IllegalCharacterError
                                # ILLEGAL_CHARACTERS_RE = re.compile(r'[\000-\010]|[\013-\014]|[\016-\037]|\xef|\xbf')
                                # item = ILLEGAL_CHARACTERS_RE.sub('', item)
                        i += 1
        if len(wb.sheetnames) > 1:
            wb = self.del_sheet(wb, [r"Sheet"])
        wb.save(self.source_file)
        wb.close()

    def read_excel_xlsx(self) -> dict:
        dict_sheet = {}
        wb = openpyxl.load_workbook(self.source_file, data_only=True)
        # 遍历sheet
        for sheet in wb.sheetnames:
            # worksheet = wb.get_sheet_by_name(sheet)
            worksheet = wb[sheet]
            list_sheet = []
            for i in range(worksheet.max_row):
                fileLine = []
                for j in range(worksheet.max_column):
                    cell_value = worksheet.cell(row=i + 1, column=j + 1).value
                    cell_value = Data().data_type_conversion(cell_value)
                    cell_value = Data().data_value_conversion(cell_value)
                    # logging.debug(cell_value)
                    fileLine.append(cell_value)
                list_sheet.append(fileLine)
            dict_sheet[sheet] = list_sheet
        wb.close()
        return dict_sheet

    def read_excel_xlsx_by_sheet(self, sheet_name) -> list:
        list_sheet = []
        try:
            wb = openpyxl.load_workbook(self.source_file, data_only=True)

            # 获取一个工作表
            if len(wb.sheetnames) > 1:
                worksheet = wb[sheet_name]  # 通过名称获取
            else:
                worksheet = wb[wb.sheetnames[0]]  # 通过索引顺序获取

            # 获取行数和列数
            # allrows = sheet.max_row
            # allcols = sheet.max_column
            # logging.debug("%s, %s, %s, %s" % (allrows, allcols, sheet_name, source_file))

            for i in range(worksheet.max_row):
                fileLine = []
                for j in range(worksheet.max_column):
                    cell_value = worksheet.cell(row=i + 1, column=j + 1).value
                    cell_value = Data().data_type_conversion(cell_value)
                    cell_value = Data().data_value_conversion(cell_value)
                    # logging.debug(cell_value)
                    fileLine.append(cell_value)
                list_sheet.append(fileLine)
        except Exception as e:
            print(colorama.Fore.RED + str(e) + colorama.Style.RESET_ALL)
        finally:
            wb.close()
        return list_sheet


class Excel_XLS(FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name=GLOBAL_XLSX_FILE):
        super(Excel_XLS, self).__init__(file_name)
        self.source_file = os.path.abspath(file_name)

    def create_excel_xls(self, dict_sheet, append=False, column_width_auto=True):
        wb = xlwt.Workbook()
        for key, value in dict_sheet.items():
            if value != None and len(value) > 0:
                workSheet = wb.add_sheet(key)

                for i, fileLine in enumerate(value):
                    if len(str(fileLine)) > 0:
                        for j, item in enumerate(fileLine):
                            # logging.debug(item)
                            workSheet.write(i, j, str(item))
                            j += 1
                        i += 1
        wb.save(self.source_file)

    def read_excel_xls(self) -> dict:
        dict_sheet = {}
        wb = xlrd.open_workbook(self.source_file)
        # 遍历sheet
        for sheet in wb.sheets():
            list_sheet = []
            for row in range(sheet.nrows):
                fileLine = []
                for col in range(sheet.ncols):
                    cell_value = sheet.cell(row, col).value
                    cell_value = Data().data_type_conversion(cell_value)
                    cell_value = Data().data_value_conversion(cell_value)
                    # logging.debug(cell_value)
                    fileLine.append(cell_value)
                list_sheet.append(fileLine)
            dict_sheet[sheet.name] = list_sheet
        return dict_sheet

    def read_excel_xls_by_sheet(self, sheet_name) -> list:
        list_sheet = []
        try:
            wb = xlrd.open_workbook(self.source_file)

            # 获取一个工作表
            if wb.sheets().__len__() > 1:
                sheet = wb.sheet_by_name(sheet_name)  # 通过名称获取
            else:
                sheet = wb.sheets()[0]  # 通过索引顺序获取
            # sheet = data.sheet_by_index(0)         #通过索引顺序获取

            # 获取行数和列数
            # allrows = sheet.nrows
            # allcols = sheet.ncols
            # logging.debug("%s, %s, %s, %s" % (allrows, allcols, sheet_name, source_file))

            for row in range(sheet.nrows):
                fileLine = []
                for col in range(sheet.ncols):
                    cell_value = sheet.cell(row, col).value
                    cell_value = Data().data_type_conversion(cell_value)
                    cell_value = Data().data_value_conversion(cell_value)
                    # logging.debug(cell_value)
                    fileLine.append(cell_value)
                list_sheet.append(fileLine)
        except Exception as e:
            print(colorama.Fore.RED + str(e) + colorama.Style.RESET_ALL)
        return list_sheet


class Excel(Excel_XLSX, Excel_XLS, FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name=GLOBAL_XLSX_FILE):
        super(Excel, self).__init__(file_name)
        self.source_file = os.path.abspath(file_name)

    def create_excel(self, dict_sheet, append=False, column_width_auto=True, engine="openpyxl"):
        suffix = self.get_file_suffix()
        if dict_sheet != None and len(dict_sheet) > 0:
            if ".xlsx" == suffix:
                Excel_XLSX(self.source_file).create_excel_xlsx(dict_sheet, append, column_width_auto)
            elif ".xls" == suffix:
                Excel_XLS(self.source_file).create_excel_xls(dict_sheet, append, column_width_auto)
            else:
                if engine == 'xlwt':
                    self.source_file += ".xls"
                    Excel_XLS(self.source_file).create_excel_xls(dict_sheet, append, column_width_auto)
                else:
                    self.source_file += ".xlsx"
                    Excel_XLSX(self.source_file).create_excel_xlsx(dict_sheet, append, column_width_auto)
            print(colorama.Fore.GREEN + "生成excel文档:", self.source_file + colorama.Style.RESET_ALL)
        else:
            print(colorama.Fore.BLUE + self.source_file, "数据为空" + colorama.Style.RESET_ALL)

    # 每个sheet页的结果相互依赖
    # list3D_sheet = [list_sheet_1, list_sheet_2, list_sheet_3]
    def create_excel_depend(self, dict_sheet_name, dict_head, list3D_sheet,
                            append=False, column_width_auto=True):
        dict_sheet = {}
        zipped = zip(list(dict_sheet_name.keys()), list3D_sheet)
        for choice, list_sheet in zipped:
            if list_sheet != None and len(list_sheet) > 0:
                if dict_head[choice]:
                    list_sheet.insert(0, dict_head[choice].split())
                dict_sheet[dict_sheet_name[choice]] = list_sheet

        self.create_excel(dict_sheet, append, column_width_auto)

    # 每个sheet页的结果相互独立
    def create_excel_independent(self, dict_sheet_name, dict_head,
                                 function_name, args=(),
                                 append=False, column_width_auto=True):
        dict_sheet = {}
        list_choice = list(dict_sheet_name.keys())
        for i, item in enumerate(list_choice):
            list_sheet = function_name(list_choice[i], list_choice, args=args)
            if list_sheet != None and len(list_sheet) > 0:
                list_sheet.insert(0, dict_head[list_choice[i]].split())
                dict_sheet[dict_sheet_name[list_choice[i]]] = list_sheet

        self.create_excel(dict_sheet, append, column_width_auto)

    def read_excel(self) -> dict:
        filename, suffix = os.path.splitext(self.source_file)
        if ".xlsx" == suffix:
            return Excel_XLSX(self.source_file).read_excel_xlsx()
        elif ".xls" == suffix:
            return Excel_XLS(self.source_file).read_excel_xls()
        else:
            print(colorama.Fore.BLUE + "未知的文件格式:", self.source_file + colorama.Style.RESET_ALL)
            return {}

    def read_excel_by_sheet(self, sheet_name=None) -> list:
        filename, suffix = os.path.splitext(self.source_file)
        if ".xlsx" == suffix:
            return Excel_XLSX(self.source_file).read_excel_xlsx_by_sheet(sheet_name)
        elif ".xls" == suffix:
            return Excel_XLS(self.source_file).read_excel_xls_by_sheet(sheet_name)
        else:
            print(colorama.Fore.BLUE + "未知的文件格式:", self.source_file + colorama.Style.RESET_ALL)
            return []

    # 是get方法，用@property装饰
    @property
    def sheetname(self) -> list:
        filename, suffix = os.path.splitext(self.source_file)
        if ".xlsx" == suffix:
            return openpyxl.load_workbook(self.source_file).sheetnames
        elif ".xls" == suffix:
            return xlrd.open_workbook(self.source_file).sheet_names()
        else:
            print(colorama.Fore.BLUE + "未知的文件格式:", self.source_file + colorama.Style.RESET_ALL)
            return []

    def create_excel_by_data(self, data, column_width_auto=True):
        if isinstance(data, dict):
            list3D_sheet = [list(data.items())]
        elif isinstance(data, list):
            list3D_sheet = [data]
        else:
            list3D_sheet = []

        dict_sheet_name = {
            "0": "数据字典",
        }

        dict_head = {
            "0": "",
        }

        self.create_excel_depend(dict_sheet_name, dict_head, list3D_sheet, column_width_auto)

    def readExcel(self, path, sheet=1, row=1, column="A"):
        if not isinstance(column, (str)):
            raise TypeError("column参数类型应为str类型而不是%s，'A'表示第一列以此类推" % column.__class__)
        if not isinstance(row, (int)):
            raise TypeError("row参数类型应为int类型而不是%s，'A'表示第一列以此类推" % row.__class__)
        # 读取公式的结果
        wb = openpyxl.load_workbook(path, data_only=True)
        if isinstance(sheet, (str)):
            # ws = wb.get_sheet_by_name(sheet)
            ws = wb[sheet]
            return ws[column + str(row)].value
        elif isinstance(sheet, (int)):
            # ws = wb.get_sheet_by_name(wb.sheetnames[row - 1])
            ws = wb[wb.sheetnames[row - 1]]
            return ws[column + str(row)].value
        else:
            raise TypeError("sheet参数类型应为int或str类型而不是%s，数字表示为第几个sheet页，字符表示sheet页名称" % sheet.__class__)

    # 将excel转为xml
    def excel_to_xml(self, dest_file=None, sheet_name=None, group="root", item="item", type=True, indent_flag='  '):
        if dest_file == None:
            dest_file = FileInfo(self.source_file).get_file_replace_by_suffix(".xml")
        xml_file = '<?xml version="1.0" encoding="UTF-8"?>\n'
        xml_file += '<%s>\n' % group
        dict_sheet = self.read_excel()
        if dict_sheet != None:
            for key in dict_sheet:
                if sheet_name == None:
                    pass
                else:
                    if key != sheet_name:
                        continue

                xml_file += indent_flag + '<%s>\n' % key

                # 第一行是标题行，所以排除第一行
                list_sheet = dict_sheet[key]
                for i in range(1, len(list_sheet)):
                    # 生成标签中的属性
                    if type:
                        xml_file += indent_flag * 2 + '<%s ' % item
                        tmp = ['%s="%s"' % (str(list_sheet[0][j]), str(list_sheet[i][j]))
                               for j in range(len(list_sheet[i]))]
                        xml_file += ' '.join(map(str, tmp))
                        xml_file += ' />\n'
                    else:  # 生成标签的子节点
                        xml_file += indent_flag * 2 + '<%s>\n' % item
                        tmp = [indent_flag * 3 + '<%s>%s</%s>\n' % (str(list_sheet[0][j]), str(list_sheet[i][j]), str(list_sheet[0][j]))
                               for j in range(len(list_sheet[i]))]
                        xml_file += ' '.join(map(str, tmp))
                        xml_file += indent_flag * 2 + '</%s>\n' % item

                xml_file += indent_flag + '</%s>\n' % key
        xml_file += '</%s>' % group
        ASCIIFile(dest_file).create_ASCII_file(xml_file, encoding="UTF-8")


def test_1(dest_file):
    def get_sheet_1(args=()):
        list_sheet = []
        return list_sheet

    def get_sheet_2(args=()):
        list_sheet = []
        return list_sheet

    def get_sheet_3(args=()):
        list_sheet = []
        return list_sheet

    # 每个sheet页的结果相互依赖
    def create_excel_test(dict_sheet_name, dict_head,
                          args=(), append=False, column_width_auto=True):
        list_sheet_1 = get_sheet_1(args)
        list_sheet_2 = get_sheet_2(tuple(list(args).append(list_sheet_1)))
        list_sheet_3 = get_sheet_3(tuple(list(args).append(list_sheet_2)))

        list3D_sheet = [
            list_sheet_1,
            list_sheet_2,
            list_sheet_3,
            # list_sheet_4,
            # list_sheet_5,
            # list_sheet_6,
            # list_sheet_7,
        ]

        Excel(dest_file).create_excel_depend(dict_sheet_name, dict_head, list3D_sheet, append, column_width_auto)


def test_2(dest_file):
    def get_sheet_1(args=()):
        list_sheet = []
        return list_sheet

    def get_sheet_2(args=()):
        list_sheet = []
        return list_sheet

    def get_sheet_3(args=()):
        list_sheet = []
        return list_sheet

    dict_sheet_name = {
        "0": "error",
        "1": "code",
        "2": "count",
    }

    dict_head = {
        "0": "错误号 常量定义 错误提示信息",
        "1": "功能号 行号 错误号 路径",
        "2": "错误号 使用次数",
    }

    # 每个sheet页的结果相互独立
    def get_list_sheet(choice, list_choice, args=()):
        list_sheet = []
        if choice == list_choice[0]:
            list_sheet = get_sheet_1(args)
        elif choice == list_choice[1]:
            list_sheet = get_sheet_2(args)
        elif choice == list_choice[2]:
            list_sheet = get_sheet_3(args)
        else:
            print(colorama.Fore.BLUE + "不支持该命令:%s" % choice + colorama.Style.RESET_ALL)
        return list_sheet

    Excel(dest_file).create_excel_independent(dict_sheet_name, dict_head, get_list_sheet)


def main():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xlsx")
    print(Excel(source_file).sheetname)

    # source_file = os.path.join(GLOBAL_WORK_DIR, r"config.xlsx")
    # Excel(source_file).excel_to_xml()


if __name__ == "__main__":
    main()
