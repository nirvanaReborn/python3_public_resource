#!/usr/bin/env python
# -*- coding:utf-8 -*-

import csv
# 解决python中csv文件中文写入问题
# import unicodecsv as ucsv
import colorama
import os
from public_class.public_class_FileInfo import FileInfo
from public_constant.public_resource_constant import GLOBAL_CSV_FILE


class CSV(FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name=GLOBAL_CSV_FILE):
        super(CSV, self).__init__(file_name)
        self.source_file = os.path.abspath(file_name)

    def create_csv(self, dict_sheet, dict_head, delimiter=',', append=False):
        if append:
            mode = 'a'
        else:
            mode = 'w'

        for key, value in dict_sheet.items():
            if value is not None and len(value) > 0:
                dest_file = self.source_file + '_' + str(key) + '.csv'
                # 解决python中csv文件中文写入问题
                with open(dest_file, mode, encoding='utf-8-sig', newline="") as fw:
                    if isinstance(value, list):  # 二维列表
                        # 如果我们想修改列与列之间的分隔符可以传入 delimiter 参数
                        spamwriter = csv.writer(fw, delimiter=delimiter)
                        spamwriter.writerows(value)
                    elif isinstance(value, dict):  # 字典
                        writer = csv.DictWriter(fw, fieldnames=dict_head[key].split())
                        writer.writeheader()
                        writer.writerow(value)
                    else:
                        print("不识别的类型：%s" % type(value))
                print(colorama.Fore.GREEN + "生成CSV文档:", self.source_file + colorama.Style.RESET_ALL)
            else:
                print(colorama.Fore.BLUE + self.source_file, "数据为空" + colorama.Style.RESET_ALL)

    def read_csv(self):
        list_2D = []
        with open(self.source_file, 'rt') as fr:
            for fileLine in csv.reader(fr):
                list_1D = []
                for item in fileLine:  # 每个数据（即excel中的一个单元格）
                    # print(item)
                    list_1D.append(item)
                list_2D.append(list_1D)
        return list_2D
