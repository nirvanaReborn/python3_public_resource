#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# subprocess模块被推荐用来替换一些老的模块和函数，如：os.system、os.spawn*、os.popen*等。
# subprocess模块目的是启动一个新的进程并与之通信，最常用是定义类Popen，使用Popen可以创建进程，并与进程进行复杂的交互。
# 但Popen函数存在缺陷在于，它是一个阻塞的方法，如果运行cmd命令时产生内容非常多，函数就容易阻塞；
#                         另一点，Popen方法也不会打印出cmd的执行信息。
import logging
# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
import os
import subprocess
import colorama
import chardet
import shlex
from public_class.public_class_PlatInfo import PlatInfo


class CallApp():
    # 初始化函数，类似于构造函数
    def __init__(self, file_name, arg=()):
        super(CallApp, self).__init__()
        self.source_file = os.path.abspath(file_name)
        print(self.source_file)
        self.set_arg = arg

    def call_python(self):
        interpreter = PlatInfo().get_python_exe_path()
        # interpreter = r"D:\ProgramFiles\Anaconda3\python.exe"

        log = logging.getLogger("Core.Analysis.Processing")
        if not os.path.exists(interpreter):
            log.error("找不到python解释器：[%s]." % interpreter)

        # list_arg = shlex.split(processor) # 用于linux系统测试
        list_cmd = [interpreter, self.source_file]
        list_cmd.extend(self.set_arg)
        # cwd该参数指定了子进程工作目录。这个参数很有用，有时涉及到相对路径的时候必须指定,如果不指定cwd，则程序可能出错。
        # child = subprocess.Popen(list_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) # 用于linux系统测试
        child = subprocess.Popen(list_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1)
        for line in iter(child.stdout.readline, b''):
            # print(line)
            print(colorama.Fore.CYAN + str(line, encoding='utf-8') + colorama.Style.RESET_ALL, end='')
        child.stdout.close()
        child.wait()  # 等待子进程结束


def main():
    CallApp(r"public_class_PlatInfo.py").call_python()


if __name__ == "__main__":
    main()
