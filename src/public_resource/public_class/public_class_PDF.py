#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 处理PDF文档
'''
PDF 表示Portable Document Format，使用.pdf 文件扩展名。
PyPDF2 写入PDF 的能力，仅限于从其他PDF 中拷贝页面、旋转页面、重叠页面和加密文件。
PyPDF2 不能在PdfFileWriter 对象中间插入页面，addPage()方法只能够在末尾添加页面。
pip install PyPDF2
'''
import PyPDF2
import os
from public_class.public_class_FileInfo import FileInfo
from public_constant.public_resource_constant import GLOBAL_WORK_DIR


class PDF(FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name):
        super(PDF, self).__init__(file_name)
        self.source_file = os.path.abspath(file_name)
        self.pdfFileObj = open(self.source_file, 'rb')
        self.pdfReader = PyPDF2.PdfFileReader(self.pdfFileObj)

    # 从PDF 提取文本
    def getNumPages(self):
        print("文档总页数:", self.pdfReader.numPages)
        return self.pdfReader.numPages

    # 从PDF 提取文本
    def getPdfText(self, page=None):
        # PyPDF2 在取得页面时使用从0 开始的下标：第一页是0 页，第二页是1 页，以此类推。
        if page == None:
            for page in range(len(self.pdfReader.numPages)):
                pageObj = self.pdfReader.getPage(page)
                print(pageObj.extractText())
        else:
            pageObj = self.pdfReader.getPage(page)
            print(pageObj.extractText())

        return


    # 加密PDF
    def encryptPDF(self,dest_file, user_pwd, owner_pwd = None):
        pdfWriter = PyPDF2.PdfFileWriter()
        for pageNum in range(self.pdfReader.numPages):
            pdfWriter.addPage(self.pdfReader.getPage(pageNum))

        # PDF 可以有一个用户口令（允许查看这个PDF）和一个拥有者口令（允许设置打印、注释、提取文本和其他功能的许可）。
        # 用户口令和拥有者口令分别是encrypt()的第一个和第二个参数。
        # 如果只传入一个字符串给encrypt()，它将作为两个口令。
        pdfWriter.encrypt(user_pwd, owner_pwd)
        resultPdf = open(dest_file, 'wb')
        pdfWriter.write(resultPdf)
        resultPdf.close()


    # 解密PDF
    def decryptPDF(self, passwd):
        print(self.pdfReader.isEncrypted)     # 判断是否加密
        print(self.pdfReader.decrypt(passwd)) # 解密
        return self.getPdfText()


    # 拷贝页面
    def copyPage(self, source_file_1, source_file_2, dest_file):
        pdf1File = open(source_file_1, 'rb')
        pdf2File = open(source_file_2, 'rb')
        pdf1Reader = PyPDF2.PdfFileReader(pdf1File)
        pdf2Reader = PyPDF2.PdfFileReader(pdf2File)
        pdfWriter = PyPDF2.PdfFileWriter()
        for pageNum in range(pdf1Reader.numPages):
            pageObj = pdf1Reader.getPage(pageNum)
            pdfWriter.addPage(pageObj)

        for pageNum in range(pdf2Reader.numPages):
            pageObj = pdf2Reader.getPage(pageNum)
            pdfWriter.addPage(pageObj)

        pdfOutputFile = open(dest_file, 'wb')
        pdfWriter.write(pdfOutputFile)
        pdfOutputFile.close()
        pdf1File.close()
        pdf2File.close()


    # 叠加页面
    def superpositionPage(self, source_file_1, source_file_2, dest_file):
        pdf1File = open(source_file_1, 'rb')
        pdfReader = PyPDF2.PdfFileReader(pdf1File)
        minutesFirstPage = pdfReader.getPage(0)

        # 获取水印
        pdf2File = open(source_file_2, 'rb')
        pdfWatermarkReader = PyPDF2.PdfFileReader(pdf2File)
        minutesFirstPage.mergePage(pdfWatermarkReader.getPage(0))

        pdfWriter = PyPDF2.PdfFileWriter()
        pdfWriter.addPage(minutesFirstPage)
        for pageNum in range(1, pdfReader.numPages):
            pageObj = pdfReader.getPage(pageNum)
            pdfWriter.addPage(pageObj)
        resultPdfFile = open(dest_file, 'wb')
        pdfWriter.write(resultPdfFile)
        pdf1File.close()
        resultPdfFile.close()


    # 旋转页面(degrees传入整数90、180 或270 就可以了)
    def rotatingPage(self, dest_file, degrees):
        page = self.pdfReader.getPage(0)
        page.rotateClockwise(degrees)
        # page.rotateCounterClockwise(degrees)
        pdfWriter = PyPDF2.PdfFileWriter()
        pdfWriter.addPage(page)
        resultPdfFile = open(dest_file, 'wb')
        pdfWriter.write(resultPdfFile)
        resultPdfFile.close()
        self.pdfFileObj.close()


    # 关闭页面
    def close(self):
        self.pdfFileObj.close()


def main():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"demo.pdf")
    obj = PDF(source_file)
    obj.getNumPages()
    obj.get_pdfText()


if __name__ == "__main__":
    main()
