#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
import sys
import os


class DirTree():
    def __init__(self):
        self.SPACE = ""
        self.list = []

    def getCount(self, url):
        files = os.listdir(url)
        count = 0
        for file in files:
            myfile = url + os.sep + file
            if os.path.isfile(myfile):
                count = count + 1
        return count

    def generate_tree(self, url):
        files = os.listdir(url)
        fileNum = self.getCount(url)
        tmpNum = 0
        for file in files:
            myfile = url + os.sep + file
            if os.path.isfile(myfile):
                file_size = os.path.getsize(myfile)
                file_size = round(file_size / float(1024 * 1024), 2)
                tmpNum = tmpNum + 1
                if (tmpNum != fileNum):
                    self.list.append(str(self.SPACE) + "├----" + file + ' : ' + str(file_size) + 'M' + "\n")
                else:
                    self.list.append(str(self.SPACE) + "└----" + file + ' : ' + str(file_size) + 'M' + "\n")
            elif os.path.isdir(myfile):
                self.list.append(str(self.SPACE) + "├----" + file + "\n")
                # change into sub directory
                self.SPACE = self.SPACE + "│ "
                self.generate_tree(myfile)
                # if iterator of sub directory is finished, reduce "│ "
                self.SPACE = self.SPACE[:-4]
        return self.list

    def save_file(self, dest_file):
        with open(dest_file, 'w', encoding='gbk') as f:
            f.writelines(self.list)


def test_1():
    d = DirTree()
    d.generate_tree(r'D:\SVN\PBOX\trunk\SourcesEx\FBase2Plugin')  # input directory
    d.save_file("D:/1.txt")  # write to file


# ------------------------------------------------------------------------------------
# https://yq.aliyun.com/ziliao/132265
import optparse

LOCATION_NONE = 'NONE'
LOCATION_MID = 'MID'
LOCATION_MID_GAP = 'MID_GAP'
LOCATION_TAIL = 'TAIL'
LOCATION_TAIL_GAP = 'TAIL_GAP'

Notations = {
    LOCATION_NONE: '',
    LOCATION_MID: '├─',
    LOCATION_MID_GAP: '│  ',
    LOCATION_TAIL: '└─',
    LOCATION_TAIL_GAP: '    '
}


class Node(object):
    def __init__(self, name, depth, parent=None, location=LOCATION_NONE):
        self.name = name
        self.depth = depth
        self.parent = parent
        self.location = location
        self.children = []

    def __str__(self):
        sections = [self.name]
        parent = self.has_parent()
        if parent:
            if self.is_tail():
                sections.insert(0, Notations[LOCATION_TAIL])
            else:
                sections.insert(0, Notations[LOCATION_MID])
            self.__insert_gaps(self, sections)
        return ''.join(sections)

    def __insert_gaps(self, node, sections):
        parent = node.has_parent()
        # parent exists and parent's parent is not the root node
        if parent and parent.has_parent():
            if parent.is_tail():
                sections.insert(0, Notations[LOCATION_TAIL_GAP])
            else:
                sections.insert(0, Notations[LOCATION_MID_GAP])
            self.__insert_gaps(parent, sections)

    def has_parent(self):
        return self.parent

    def has_children(self):
        return self.children

    def add_child(self, node):
        self.children.append(node)

    def is_tail(self):
        return self.location == LOCATION_TAIL


class Tree(object):
    def __init__(self):
        self.nodes = []

    def debug_print(self):
        for node in self.nodes:
            print(str(node) + os.sep)

    def write2file(self, filename):
        try:
            with open(filename, 'w') as fp:
                fp.writelines(str(node) + os.sep + '\n'
                              for node in self.nodes)
        except IOError as e:
            print(e)
            return 0
        return 1

    def build(self, path):
        self.__build(path, 0, None, LOCATION_NONE)

    def __build(self, path, depth, parent, location):
        if os.path.isdir(path):
            name = os.path.basename(path)
            node = Node(name, depth, parent, location)
            self.add_node(node)
            if parent:
                parent.add_child(node)

            entries = self.list_folder(path)
            end_index = len(entries) - 1
            for i, entry in enumerate(entries):
                childpath = os.path.join(path, entry)
                location = LOCATION_TAIL if i == end_index else LOCATION_MID
                self.__build(childpath, depth + 1, node, location)

    def list_folder(self, path):
        """Folders only."""
        return [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]
        # for entry in os.listdir(path):
        #     childpath = os.path.join(path, entry)
        #     if os.path.isdir(childpath):
        #         yield entry

    def add_node(self, node):
        self.nodes.append(node)


def _parse_args():
    parser = optparse.OptionParser()
    parser.add_option(
        '-p', '--path', dest='path', action='store', type='string',
        default='./', help='the path to generate the tree [default: %default]')
    parser.add_option(
        '-o', '--out', dest='file', action='store', type='string',
        help='the file to save the result [default: pathname.trees]')
    options, args = parser.parse_args()
    # positional arguments are ignored
    return options


def test_2():
    options = _parse_args()
    path = options.path
    if not os.path.isdir(path):
        print('%s is not a directory' % path)
        return 2

    if not path or path == './':
        filepath = os.path.realpath(__file__)  # for linux
        path = os.path.dirname(filepath)
    tree = Tree()
    tree.build(path)
    # tree.debug_print()
    if options.file:
        filename = options.file
    else:
        name = os.path.basename(path)
        filename = '%s.trees' % name
    return tree.write2file(filename)


# ------------------------------------------------------------------------------------
from pathlib import Path


class DirectionTree(object):
    """生成目录树
    @ pathname: 目标目录
    @ filename: 要保存成文件的名称
    """

    def __init__(self, pathname='.', filename='tree.txt', is_only_display_dir=False):
        super(DirectionTree, self).__init__()
        self.pathname = Path(pathname)
        self.filename = filename
        self.tree = ''
        self.is_only_display_dir = is_only_display_dir

    def set_path(self, pathname):
        self.pathname = Path(pathname)

    def set_filename(self, filename):
        self.filename = filename

    def generate_tree(self, level=0):
        if self.pathname.is_file():
            if self.is_only_display_dir:
                pass
            else:
                file_size = os.path.getsize(self.pathname)
                file_size = round(file_size / float(1024 * 1024), 2)
                self.tree += '    |' * level + '-' * 4 + self.pathname.name + ':' + str(file_size) + 'M' + '\n'
        elif self.pathname.is_dir():
            self.tree += '    |' * level + '-' * 4 + \
                         str(self.pathname.relative_to(self.pathname.parent)) + os.sep + '\n'

            for subdir in self.pathname.iterdir():
                self.pathname = Path(subdir)
                self.generate_tree(level + 1)

    def save_file(self):
        with open(self.filename, 'w', encoding='utf-8') as f:
            f.write(self.tree)


def test_3():
    dirtree = DirectionTree(is_only_display_dir=False)
    # 命令参数个数为1，生成当前目录的目录树
    if len(sys.argv) == 1:
        # dirtree.set_path(pathname=Path.cwd())
        dirtree.set_path(pathname=r"D:\SVN\PBOX\trunk\SourcesEx\FBase2Plugin")
        dirtree.generate_tree()
        print(dirtree.tree)
    # 命令参数个数为2并且目录存在存在
    elif len(sys.argv) == 2 and Path(sys.argv[1]).exists():
        dirtree.set_path(sys.argv[1])
        dirtree.generate_tree()
        print(dirtree.tree)
    # 命令参数个数为3并且目录存在存在
    elif len(sys.argv) == 3 and Path(sys.argv[1]).exists():
        dirtree.set_path(sys.argv[1])
        dirtree.generate_tree()
        dirtree.set_filename(sys.argv[2])
        dirtree.save_file()
    else:  # 参数个数太多，无法解析
        print('命令行参数太多，请检查！')


# ------------------------------------------------------------------------------------
# 树形目录显示
# pip install filestools -U
def test_4(source_dir):
    '''
    关于tree_dir()函数，分别介绍如下3个参数：
    path：递归显示的目录路径，默认为当前目录；
    m_level：递归展示的最大层数，默认为7层；
    no_calc：指定该参数后，对于超过递归显示的最大层数的文件夹，不再继续递归计算文件夹大小；
    '''
    from treedir.tree import tree_dir
    tree_dir(source_dir, m_level=7, no_calc=False)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_" + str(i) + "()"

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    elif choice == 0:
        sys.exit()
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    sys.exit(main())
