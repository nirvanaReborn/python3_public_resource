#!/usr/bin/env python3

"""
https://zhuanlan.zhihu.com/p/465056939
评论：我实现的股票数据存储: 股票日线共64字节，作为文件的第一部分直接写文件；股票当日tick是个 ndarry，用lzma压缩后，作为文件第二部分写入。这样写文件稍微慢点，但是读起来比文章中所有文件格式都快。

Jay格式在读取和写入性能上都是最优的,这是因为Jay格式编码采用高效的numpy二进制格式,并且 format本身也很简单。
综合所有的测试报告,对此数据集来说Jay格式无疑是性能最高的格式选择。
如果要考虑格式的可交互性,则可以选择Feather或者Parquet格式,它们也有比较优异的性能表现。

https://www.xjx100.cn/news/13271.html?action=onClick
可交互性指的数据格式是否可以方便地在不同系统、语言或工具间交换使用。
一个好的例子就是Parquet格式,它有以下几个方面的可交互性:
1. 跨语言:Parquet格式文件可以用Java、Python、C++等多种语言读和写,不限定于某一语言。
2. 兼容不同工具:主流的工具如Apache Spark、Apache Drill、Apache Hive等都支持Parquet格式,可以直接操作Parquet文件。
3. 自描述性:Parquet文件的元数据是存储在文件中,这样工具和语言在不了解文件格式的情况下也可以正确读取数据,提高兼容性。
4. 列式存储:Parquet的列式存储方式很容易被其他系统理解和使用。
5. 压缩:Parquet使用的压缩算法也比较通用,如Snappy、GZIP等。
而相比之下,像Pickle、HDF5、Feather格式等就不及Parquet格式那么具有可交互性。因为:
1. 这些格式的读写通常限定于使用Python。
2. 文件格式的元数据和编码格式都比较复杂,不太容易被其他外部系统理解,而且也不提供自描述性。
3. 这些格式的压缩算法比较Python特有,外部系统难以支持。
4. 存储方式也比较Python特有,如Pickle使用Python对象序列化,难以被其他语言解析。
所以,总体来说,Parquet、ORC、Avro这些格式具有更好的可交互性,而Pickle、HDF5、Feather等格式可交互性较差,更加Python专用。
"""
# tabulate默认没有考虑中文字符宽度，因此无法对齐，如要实现对齐，需要wcwidth 包。
from tabulate import tabulate
# import wcwidth
# from numba import jit
# https://datatable.readthedocs.io/en/latest/?badge=latest
import datatable as dt
import numpy as np
import pandas as pd
import sqlite3
import csv
import yaml
import traceback
import time
import psutil
import os
import sys
import io
import fcntl

# import mars.dataframe as md
from ..public_function.public_check_timeout import check_timeout

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')

tabulate.PRESERVE_WHITESPACE = True  # 保留空格
tabulate.WIDE_CHARS_MODE = False  # 宽（全角中日韩）符号

dict_file_format = {
    '.jay': 'jay',
    '.parquet': 'parquet',
    '.pk': 'pickle',
    # '.pickle': 'pickle',
    '.feather': 'feather',
    '.hdf5': 'hdf5',
    '.db': 'sqlite',
    '.json': 'json',
    '.yaml': 'yaml',
    '.xml': 'xml',
    '.html': 'html',
    '.csv': 'csv',
    '.xlsx': 'excel',
    # '.xls': 'excel',
    # '.odf': 'excel',
    # '.ods': 'excel',
    # '.odt': 'excel',
}


class FileIO:
    def __init__(self, file_path, file_format=None):
        self.file_path = os.path.abspath(file_path)
        # 目录路径、文件名称
        self.dir_path, self.file_name = os.path.split(self.file_path)
        # 文件尾缀
        self.shotname, self.extension = os.path.splitext(self.file_name)
        # 文件格式
        if file_format:
            self.file_format = file_format
        else:
            self.file_format = dict_file_format.get(self.extension, None)

        if self.file_format == 'sqlite':
            # 创建数据库连接
            self.conn = sqlite3.connect('test.db')

    def get_excel_engine(self, mode='r'):
        if self.extension == '.xlsx':  # 支持新的 Excel 文件格式
            return 'openpyxl'
        elif self.extension == '.xls':  # 支持旧式 Excel 文件：最大写入65536行数据
            return 'xlrd' if mode == 'r' else 'xlwt'
        elif self.extension in ['.odf', '.ods', '.odt']:  # 支持 OpenDocument 文件格式
            return 'odf'
        else:  # 支持二进制 Excel 文件
            return 'pyxlsb'

    def read(self, **kwargs):
        try:
            if self.file_format == 'jay':
                return dt.fread(self.file_path, **kwargs)
            elif self.file_format == 'parquet':
                return pd.read_parquet(self.file_path, **kwargs)
            elif self.file_format == 'pickle':
                return pd.read_pickle(self.file_path, **kwargs)
            elif self.file_format == 'feather':
                return pd.read_feather(self.file_path, **kwargs)
            elif self.file_format == 'hdf5':
                return pd.read_hdf(self.file_path, **kwargs)
            elif self.file_format == 'sqlite':
                kwargs.update({'sql': 'select * from table_name',
                               'con': self.conn})
                return pd.read_sql(**kwargs)
            elif self.file_format == 'json':
                return pd.read_json(self.file_path, **kwargs)
            elif self.file_format == 'yaml':
                # 设置以utf-8解码模式读取文件，encoding参数必须设置，
                # 否则默认以gbk模式读取文件，当文件中包含中文时，会报错
                encoding = kwargs.pop("encoding", "utf-8")
                return yaml.load(open(self.file_path, 'r', encoding=encoding), Loader=yaml.FullLoader)
            elif self.file_format == 'xml':
                return pd.read_xml(self.file_path, **kwargs)
            elif self.file_format == 'html':
                return pd.read_html(self.file_path, **kwargs)
            elif self.file_format == 'csv':
                # https://zhuanlan.zhihu.com/p/68652512
                # return pd.read_csv(self.file_path, **kwargs)
                # return md.read_csv(self.file_path, **kwargs).execute()
                return dt.fread(self.file_path, **kwargs).to_pandas(**kwargs)
            elif self.file_format == 'excel':
                kwargs.update(self.get_excel_engine())
                return pd.read_excel(self.file_path, **kwargs)
            else:
                print('{0}文件不支持读取'.format(self.file_path))
                return pd.DataFrame()
        except Exception as e:
            print('{0}文件读取报错:{1}'.format(self.file_path, e))
            return pd.DataFrame()

    def write(self, df, **kwargs):
        try:
            if self.file_format == 'jay':
                dt.Frame(df).to_jay(self.file_path, **kwargs)
            elif self.file_format == 'parquet':
                df.to_parquet(self.file_path, **kwargs)
            elif self.file_format == 'pickle':
                df.to_pickle(self.file_path, **kwargs)
            elif self.file_format == 'feather':
                df.to_feather(self.file_path, **kwargs)
            elif self.file_format == 'hdf5':
                kwargs.update({'key': 'df'})
                df.to_hdf(self.file_path, **kwargs)
            elif self.file_format == 'sqlite':
                kwargs.update({'con': self.conn,
                               'if_exists': 'replace',
                               'index': False})
                return df.to_sql('table_name', **kwargs)
            elif self.file_format == 'json':
                df.to_json(self.file_path, **kwargs)
            elif self.file_format == 'yaml':
                # allow_unicode=True 表示正常显示中文
                return yaml.dump(df, stream=open(self.file_path, 'w', encoding="utf-8"), sort_keys=True,
                                 allow_unicode=True, Dumper=yaml.RoundTripDumper, **kwargs)
            elif self.file_format == 'xml':
                df.to_xml(self.file_path, **kwargs)
            elif self.file_format == 'html':
                df.to_html(self.file_path, **kwargs)
            elif self.file_format == 'csv':
                df.to_csv(self.file_path, **kwargs)
            elif self.file_format == 'excel':
                kwargs.update({'engine': self.get_excel_engine('w'),
                               'header': False,
                               'index': False})
                return df.to_excel(self.file_path, **kwargs)
            else:
                print('{0}文件不支持写入'.format(self.file_path))
        except Exception as e:
            print('{0}文件写入报错:{1}'.format(self.file_path, e))
            sys.exit()

    def safe_read(self, **kwargs):
        """
        加共享锁方式读取文件。目前支持的文件类型：1.csv，需要传入return_type(返回数据类型)入参，支持：dataframe(默认)；csv_reader；dict_reader。
        :param kwargs: 读取文件的参数(dict)
        :return: 读取的文件内容(csv：DataFrame/csv_reader/dict_reader)
        """
        if os.path.exists(self.file_path):
            with open(self.file_path, mode="r", encoding="utf-8") as fr:
                fcntl.flock(fr.fileno(), fcntl.LOCK_SH)

                @check_timeout
                def _safe_read():
                    try:
                        if self.file_format == "csv":
                            return_type = kwargs.pop("return_type", "dataframe")
                            if return_type == "dataframe":
                                df = pd.read_csv(fr, **kwargs)
                                fcntl.flock(fr.fileno(), fcntl.LOCK_UN)
                                return df
                            elif return_type == "csv_reader":
                                csv_reader = list(csv.reader(fr))
                                fcntl.flock(fr.fileno(), fcntl.LOCK_UN)
                                return csv_reader
                            elif return_type == "dict_reader":
                                file_content = fr.read()
                                dict_reader = csv.DictReader(io.StringIO(file_content))
                                fcntl.flock(fr.fileno(), fcntl.LOCK_UN)
                                return dict_reader
                            else:
                                logging.error("%s文件不支持的读取类型%s" % (self.file_name, return_type))
                                fcntl.flock(fr.fileno(), fcntl.LOCK_UN)
                                return None
                        else:
                            logging.error("%s文件不支持读取" % self.file_name)
                            fcntl.flock(fr.fileno(), fcntl.LOCK_UN)
                            return None
                    except BaseException:
                        logging.error("%s文件读取报错，错误原因：%s" % (self.file_name, traceback.format_exc()))
                        fcntl.flock(fr.fileno(), fcntl.LOCK_UN)
                        return None

                data = _safe_read()
                if data is None:
                    try:
                        fcntl.flock(fr.fileno(), fcntl.LOCK_UN)
                    except BaseException:
                        logging.debug("文件%s锁释放失败，错误原因：%s" % (self.file_path, traceback.format_exc()))
                return data

    def safe_write(self, data, **kwargs):
        """
        加独占锁方式写入文件，需要传入mode(写入类型)入参：a，增量；w，清空。目前支持的文件类型：1.csv，需要传入data_type(保存数据类型)入参，支持：dataframe(默认)；dict，需要传入column_names(列名)；list。
        :param data: 待写入的文件内容(csv：DataFrame/csv_reader/dict_reader)
        :return: 是否保存成功(bool)
        """
        mode = kwargs.pop("mode", "w")
        with open(self.file_path, mode=mode, encoding="utf-8") as fw:
            fcntl.flock(fw.fileno(), fcntl.LOCK_EX)

            @check_timeout
            def _safe_write():
                try:
                    if self.file_format == "csv":
                        data_type = kwargs.pop("data_type", "dataframe")
                        if data_type == "dataframe":
                            data.to_csv(fw, **kwargs)
                            fcntl.flock(fw.fileno(), fcntl.LOCK_UN)
                            return True
                        elif data_type == "dict":
                            column_names = kwargs.pop("column_names", None)
                            if column_names is None:
                                logging.error("未传入列名，无法执行写入操作")
                                fcntl.flock(fw.fileno(), fcntl.LOCK_UN)
                                return False
                            else:
                                dict_writer = csv.DictWriter(fw, fieldnames=column_names, **kwargs)
                                dict_writer.writeheader()
                                dict_writer.writerows(data)
                                fcntl.flock(fw.fileno(), fcntl.LOCK_UN)
                                return True
                        elif data_type == "list":
                            csv_writer = csv.writer(fw)
                            csv_writer.writerows(data)
                            fcntl.flock(fw.fileno(), fcntl.LOCK_UN)
                            return True
                        else:
                            logging.error("%s文件不支持的写入类型%s" % (self.file_name, data_type))
                            fcntl.flock(fw.fileno(), fcntl.LOCK_UN)
                            return False
                    else:
                        logging.error("%s文件不支持写入" % self.file_name)
                        fcntl.flock(fw.fileno(), fcntl.LOCK_UN)
                        return False
                except BaseException:
                    logging.error("%s文件写入报错，错误原因：%s" % (self.file_name, traceback.format_exc()))
                    fcntl.flock(fw.fileno(), fcntl.LOCK_UN)
                    return False

            data = _safe_write()
            if data is None:
                try:
                    fcntl.flock(fw.fileno(), fcntl.LOCK_UN)
                except BaseException:
                    logging.debug("文件%s锁释放失败，错误原因：%s" % (self.file_path, traceback.format_exc()))
            return data

    def test_performance(self, func, args=()):
        start = time.time()
        func(*args)
        end = time.time()
        take_time = end - start

        # 每0.5秒获取一次CPU和内存使用数据,并取平均值
        cpu_usage = []
        memory_usage = []
        for _ in range(int(take_time / 0.5)):
            # 获得CPU使用率
            cpu_usage.append(psutil.cpu_percent())
            # 获得内存使用率
            memory_usage.append(psutil.virtual_memory().percent)
        cpu_usage = np.mean(cpu_usage)
        memory_usage = np.mean(memory_usage)
        if os.path.exists(self.file_path):
            # file_size = psutil.disk_usage(self.file_path).used
            file_size = os.path.getsize(self.file_path)
        else:
            file_size = -1
        # print("{0}——{1}:file_size={2},take_time={3},cpu_usage={4},memory_usage={5}".format(
        #     self.file_path, func.__name__, file_size, take_time, cpu_usage, memory_usage))
        return take_time, cpu_usage, memory_usage, file_size

    def test_read(self):
        return self.test_performance(self.read)

    def test_write(self, rows=65536, cols=5):
        # 测试数据,行数和列数可自定义
        df = pd.DataFrame(np.random.randn(rows, cols), columns=['col_' + str(i) for i in range(cols)])
        return self.test_performance(self.write, args=[df])

    @staticmethod
    def batch_test(rows=65536, cols=5):
        str_head = 'file_format write_time write_cpu write_memory file_size read_time read_cpu read_memory'
        list_2d = []
        for key in dict_file_format:
            source_file = "test" + key
            file_io = FileIO(source_file)
            list_temp = [key]
            list_temp.extend(list(file_io.test_write(rows, cols)))
            list_temp.extend(list(file_io.test_read())[:-1])
            list_2d.append(list_temp)
        # print(list_2d)
        print(tabulate(list_2d, headers=str_head.split(), tablefmt='fancy_grid'))
        return list_2d


def test_1():
    """测试性能"""
    FileIO.batch_test()


def test_2():
    """格式相互转换"""
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_" + str(i) + "()"

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
