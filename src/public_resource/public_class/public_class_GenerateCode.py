#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 自动生成脚本代码

import logging
# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
from public_class.public_class_Excel import Excel
from public_class.public_class_ASCIIFile import ASCIIFile


class GenerateCode():
    # 初始化函数，类似于构造函数
    def __init__(self, source_file, dest_file):
        super(GenerateCode, self).__init__()
        self.source_file = source_file
        self.dest_file = dest_file


    def generate_code_sql(self, generate_code_add, generate_code_del, sheet_name=None):
        sql = ""
        list_sheet = Excel(self.source_file).read_excel_by_sheet(sheet_name)
        for i, fileLine in enumerate(list_sheet):
            # print(fileLine)
            if fileLine[0] == "add" or fileLine[0] == "mod":
                sql += generate_code_add(fileLine)
            elif fileLine[0] == "del":
                sql += generate_code_del(fileLine)
            else:
                continue
        ASCIIFile(self.dest_file).create_ASCII_file(sql)


def main():
    pass


if __name__ == "__main__":
    main()