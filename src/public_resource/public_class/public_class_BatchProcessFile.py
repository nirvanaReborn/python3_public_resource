#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 批量处理文件

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
import os
from public_class.public_class_Traverse import Traverse
from public_class.public_class_FileInfo import FileInfo


class BatchProcessFile(Traverse):
    # 初始化函数，类似于构造函数
    def __init__(self, source_dir):
        super(BatchProcessFile, self).__init__(source_dir)
        self.source_dir = source_dir

    # 将当前目录下所有文件的文件名中的某个敏感词删除
    def batch_del_sensitiveWords(self, key):
        list_file = Traverse(self.source_dir).filter_file()
        for i, file in enumerate(list_file):
            if key in file:
                tuple_file_info = FileInfo(file).get_file_path_info()
                newFileName = str(tuple_file_info[2]).replace(key, "")
                newFile = os.path.join(tuple_file_info[0], newFileName + tuple_file_info[3])
                os.rename(file, newFile)
                logging.debug("[%s]--->[%s]" % (file, newFile))
            else:
                # print(file)
                pass

    # 为当前目录下的所有文件新增后缀
    def batch_add_suffix(self, suffix):
        list_file = Traverse(self.source_dir).filter_file()
        for i, file in enumerate(list_file):
            newFile = file + suffix
            os.rename(file, newFile)
            logging.debug("[%s]--->[%s]" % (file, newFile))


def main():
    source_dir = r"D:\BaiduNetdiskDownload\1.Python标准库（中文版）"
    BatchProcessFile(source_dir)


if __name__ == "__main__":
    main()
