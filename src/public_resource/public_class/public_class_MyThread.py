#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6737971558695830020/
# 多处理和多线程是实现并行计算的两种方法，分别使用进程和线程作为处理代理。

# 进程
#     进程是正在执行的计算机程序的实例。每个进程都有自己的内存空间，用来存储正在运行的指令，以及需要存储和访问才能执行的任何数据。
#
# 线程
#     线程是进程的组件，可以并行运行。
#     一个进程中可以有多个线程，它们共享相同的内存空间，即父进程的内存空间。
#     这意味着要执行的代码以及程序中声明的所有变量将由所有线程共享。
#
# 我们知道线程共享相同的内存空间，因此必须采取特殊的预防措施，以便两个线程不会写入相同的内存位置。
# CPython 解释器使用名为 GIL(全称Global Interpreter Lock)的机制或全局解释器锁来处理这个问题。
# 其工作原理如下：
#     任何线程要执行任何函数，都必须获取全局锁。
#     一次只有一个线程可以获取该锁，这意味着解释器最终会以串行方式运行指令。
#     这种设计使得内存管理线程安全，但结果是，它根本不能利用多个 cpu 内核。
#     在单核 cpu 中，这不是什么大问题。但是如果你使用多核 cpu，这个全局锁最终会成为一个瓶颈。
#     由于 python 中 GIL 的局限性，线程不能利用多个 CPU 核实现真正的并行。
#
# 差异、优缺点:
# 线程在相同的内存空间中运行；进程有单独的内存。
# 从前面的观点来看：在线程之间共享对象更容易，但与此同时，你必须采取额外的措施来实现对象同步，以确保两个线程不会同时写入同一个对象，并且不会出现争用情况。
# 由于对象同步增加了编程开销，多线程编程更容易出现错误。另一方面，多进程编程很容易实现。
# 与进程相比，线程的开销更低；生成进程比线程花费更多的时间。
# 由于 python 中 GIL 的局限性，线程不能利用多个 CPU 核实现真正的并行。多处理没有任何这样的限制。
# 进程调度由操作系统处理，而线程调度则由 python 解释器完成。
# 子进程是可中断和可终止的，而子线程不是。你必须等待线程终止或加入。
#
# 从所有这些讨论中，我们可以得出以下结论：
#     线程应该用于涉及 IO 或用户交互的程序。
#     多处理应该用于 CPU 受限、计算密集型的程序。

# https://www.liaoxuefeng.com/wiki/897692888725344/923057623066752
# 在Thread和Process中，应当优选Process，因为Process更稳定，并且Process可以分布到多台机器上；而Thread最多只能分布到同一台机器的多个CPU上。
# Python的multiprocessing模块不但支持多进程，其中managers子模块还支持把多进程分布到多台机器上。
# 一个服务进程可以作为调度者，将任务分布到其他多个进程中，依靠网络通信。
# 由于managers模块封装很好，不必了解网络通信的细节，就可以很容易地编写分布式多进程程序。

# multiprocessing：(Python 标准库) 基于进程的“线程”接口。[官网](https://docs.python.org/2/library/multiprocessing.html)
# threading：(Python 标准库)更高层的线程接口。[官网](https://docs.python.org/2/library/threading.html)
# eventlet：支持 WSGI 的异步框架。[官网](http://eventlet.net/)
# gevent：一个基于协程的 Python 网络库，使用 [greenlet](https://github.com/python-greenlet/greenlet)。[官网](http://www.gevent.org/)
# Tomorrow：用于产生异步代码的神奇的装饰器语法实现。[官网](https://github.com/madisonmay/Tomorrow)
# uvloop：在 libuv 之上超快速实现 asyncio 事件循环。[官网](https://github.com/MagicStack/uvloop)

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
import threading
import multiprocessing


# 多线程
class MyThread(threading.Thread):
    def __init__(self, func, args=()):
        super(MyThread, self).__init__()
        self.func = func
        self.args = args

    def run(self):
        self.result = self.func(*self.args)

    def get_result(self):
        try:
            # 如果子线程不使用join方法，此处可能会报没有self.result的错误
            return self.result
        except Exception:
            return None


# 多进程
class MyProcess(multiprocessing.Process):
    def __init__(self, func, args=()):
        super(MyProcess, self).__init__()
        self.func = func
        self.args = args

    def run(self):
        self.result = self.func(*self.args)

    def get_result(self):
        try:
            # 如果子线程不使用join方法，此处可能会报没有self.result的错误
            return self.result
        except Exception:
            return None


def main():
    import os
    print("多核CPU", os.cpu_count())


if __name__ == "__main__":
    main()
