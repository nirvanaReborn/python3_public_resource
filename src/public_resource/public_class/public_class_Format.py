#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 格式化数据、美化显示二维列表

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')


class Format():
    # 初始化函数，类似于构造函数
    def __init__(self):
        super(Format, self).__init__()

    # -------------------------------------------------------------------------------------
    # http://blog.csdn.net/codeway3d/article/details/52798804
    # noinspection PyProtectedMember
    def show_PrettyTable(self, list_2D, list_head=None, title=None):
        import prettytable

        if not list_2D:
            return

        if title:
            print(title)

        if isinstance(list_2D, dict):
            list_2D = [list_2D]

        table = prettytable.PrettyTable(border=True, header=True)
        # noinspection PyProtectedMember
        table.align = "l"  # 水平对齐方式（None，“l”（左对齐），“c”（居中），“r”右对齐）
        for i, fileLine in enumerate(list_2D):
            if fileLine:
                if isinstance(fileLine, dict):
                    table._set_field_names(fileLine.keys())
                    table.add_row(fileLine.values())
                else:
                    table.add_row(fileLine)

        if list_head:
            table._set_field_names(list_head)  # Field names must be unique!
        print(table)

    def show_PrettyTable_1D(self, list_1D):
        import itertools
        list_head = ["标题"]
        self.show_PrettyTable(list(map(list, itertools.zip_longest(*[list_1D]))), list_head)

    # -------------------------------------------------------------------------------------
    def show_tablib(self, list_2D, list_head=None):
        import tablib
        import os
        dataset2 = tablib.Dataset(*list_2D, headers=list_head)
        print('dataset2: ', os.linesep, dataset2, os.linesep)

    def show_tablib_1D(self, list_1D):
        import itertools
        list_head = ["标题"]
        self.show_tablib(list(map(list, itertools.zip_longest(*[list_1D]))), list_head)

    # -------------------------------------------------------------------------------------
    def show_pretty(self, list_2D, list_head=None):
        import prettyprinter
        print(prettyprinter.get_default_config())
        prettyprinter.cpprint(list_2D)

    # -------------------------------------------------------------------------------------
    def format_Chinese(self, data, size):
        count = 0
        for s in data:
            if ord(s) > 127:
                count += 1
        newStr = '{0:{wd}}'.format(data, wd=size - count)
        return newStr

    def format_list(self, list_1D):
        for i in range(len(list_1D)):
            newStr = self.format_Chinese(list_1D[i], 20)
            print('|%s|' % newStr)

    # -------------------------------------------------------------------------------------
    def is_chinese(self, uchar):
        """判断一个unicode是否是汉字"""
        if uchar >= u'\u4e00' and uchar <= u'\u9fa5':
            return True
        else:
            return False

    def align(self, text, width, just="left"):
        stext = str(text)
        cn_count = 0

        for u in stext:
            if self.is_chinese(u):
                cn_count += 2  # 计算中文字符占用的宽度
            else:
                cn_count += 1  # 计算英文字符占用的宽度

        if just == "right":
            return " " * (width - cn_count) + stext
        elif just == "left":
            return stext + " " * (width - cn_count)

    def string_ljust(self, text, width):
        return self.align(text, width, "left")

    def string_rjust(self, text, width):
        return self.align(text, width, "right")

    # -------------------------------------------------------------------------------------


def main():
    import random
    # list_2D = [[random.randint(1, 100) for col in range(5)] for row in range(6)]
    list_2D = [random.sample(range(1, 100), 5) for row in range(6)]
    Format().show_PrettyTable(list_2D)
    # Format().show_tablib(list_2D)
    # Format().show_pretty(list_2D)

    list_1D = [
        '决',
        '决决',
        '决决决',
        '决决决决',
        '决决决决决',
        '决决决决决决',
        '决决决决决决决'
    ]
    Format().format_list(list_1D)

    print("str %s|" % Format().string_rjust("a中文", 10))
    print("str %s|" % Format().string_ljust("a中文", 10))


if __name__ == "__main__":
    main()
