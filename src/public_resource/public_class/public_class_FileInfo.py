#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 获取文件信息

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
logging.getLogger("chardet").setLevel(logging.WARNING)
# pip install chardet chardet2
import chardet
import os
import stat


class FileInfo():
    # 初始化函数，类似于构造函数
    def __init__(self, file_name):
        super(FileInfo, self).__init__()
        self.source_file = os.path.abspath(file_name)

    # python读取文件编码格式
    # https://www.cnblogs.com/lizhengtan/p/5538231.html
    def get_file_encoding(self):
        """
        {'confidence': 0.64465744, 'encoding': 'utf-8'}
        confidence：表示相似度
        encoding：表示编码格式
        """
        with open(self.source_file, "rb") as fd:
            return chardet.detect(fd.read(1024))["encoding"]

    def get_file_path_info(self):
        (filepath, filename) = os.path.split(self.source_file)
        (shotname, extension) = os.path.splitext(filename)
        if extension == '.gz':
            shotname = os.path.splitext(shotname)[0]
            extension = os.path.splitext(shotname)[1] + extension
        return (filepath, filename, shotname, extension)

    def get_file_prefix(self):
        # return os.path.splitext(self.source_file)[0]

        # tmp = self.source_file.split('\\')
        # return tmp[len(tmp) - 1].split('.')[0]
        return self.get_file_path_info()[2]

    def get_file_suffix(self, has_dot=False):
        # return os.path.splitext(self.source_file)[1]  # 返回的后缀名带点
        # return self.get_file_path_info()[3]
        """
        获取文件名的后缀名
        :param has_dot: 返回的后缀名是否需要带点
        :return: 文件的后缀名
        """
        pos = self.source_file.rfind('.')
        if 0 < pos < len(self.source_file) - 1:
            index = pos if has_dot else pos + 1
            return self.source_file[index:]
        else:
            return ''

    def get_file_replace_by_suffix(self, suffix):
        filepath, filename, shotname, extension = self.get_file_path_info()
        return os.path.join(filepath, shotname + suffix)

    def get_file_info(self):
        """
        st_mode: inode 权限模式
        st_ino: inode 节点号。
        st_dev: inode 驻留的设备。
        st_nlink: inode 的链接数。
        st_uid: 所有者的用户ID。
        st_gid: 所有者的组ID。
        st_size: 普通文件以字节为单位的大小；包含等待某些特殊文件的数据。
        st_atime: 上次访问的时间。
        st_mtime: 最后一次修改的时间。
        st_ctime: 由操作系统报告的"ctime"。在某些系统上（如Unix）是最新的元数据更改的时间，在其它系统上（如Windows）是创建时间（详细信息参见平台的文档）。
        """
        # print(time.localtime(os.stat(self.source_file).st_ctime))
        # print(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(os.stat(self.source_file).st_ctime)))
        return os.stat(self.source_file)

    def is_only_read(self):
        try:
            with open(self.source_file, "r+") as fr:
                return False
        except IOError as e:
            if "[Errno 13] Permission denied" in str(e):
                return True
            else:
                print(str(e))
                return False

    def del_only_read(self):
        """
        stat.S_IREAD: windows下设为只读
        stat.S_IWRITE: windows下取消只读
        stat.S_IROTH: 其他用户有读权限
        stat.S_IRGRP: 组用户有读权限
        stat.S_IRUSR: 拥有者具有读权限
        """
        os.chmod(self.source_file, stat.S_IWRITE)  # windows下取消只读

    def rename_file(self, new_file_name):
        # 绝对路径
        os.rename(self.source_file, new_file_name)
        logging.debug("[%s]--->[%s]" % (self.source_file, new_file_name))


def main():
    file = FileInfo(r"../public_constant/public_resource_constant.py")
    print(file.get_file_encoding())
    print(file.get_file_path_info())
    print(file.get_file_prefix())
    print(file.get_file_suffix())
    print(file.get_file_replace_by_suffix(".xml"))
    print(file.get_file_info())
    print(file.is_only_read())
    # file.del_only_read()
    # file.rename_file("1.xml")


if __name__ == "__main__":
    main()
