#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
import os
import sqlite3

from public_class.public_class_Excel import Excel_Pandas
from public_class.public_class_Format import Format


class Output():
    # 初始化函数，类似于构造函数
    def __init__(self, df, file_name=None, drive="excel"):
        self.df = df
        self.dest_file = self.get_file_name(drive, file_name)
        self.dest_file = os.path.abspath(self.dest_file)
        if self.dest_file:
            self.drive = drive
        else:
            self.drive = "print"

    def get_file_name(self, drive, file_name):
        file_name = os.path.splitext(file_name)[0]
        if drive == "excel":
            return file_name + ".xlsx"
        elif drive == "sqlite":
            return file_name + ".db"
        else:
            return None

    def output_to_excel(self, sheet_name):
        # write = pandas.ExcelWriter(dest_file + ".xlsx")
        # df.to_excel(write, sheet_name, index=False, header=True, encoding='utf-8')
        # write.save()
        # write.close()

        # df.to_excel 写入不同的sheet
        dict_sheet = {}
        list_2D = self.df.values.tolist()
        list_2D.insert(0, self.df.columns.tolist())
        dict_sheet[sheet_name] = list_2D
        Excel_Pandas(self.dest_file).create_excel_pandas_by_dict(dict_sheet, index=True)

    def output_to_sqlite(self, sheet_name):
        self.df.set_index([self.df.columns.tolist()[0]], inplace=True)
        df = self.df.sort_index(axis=0, ascending=True)  # 默认按“行标签”升序排序
        # print(df.index.tolist())
        df.to_sql(sheet_name, sqlite3.connect(self.dest_file), if_exists='replace')

    def output_to_console(self, sheet_name):
        Format().show_PrettyTable(self.df.values.tolist(), self.df.columns.tolist(), sheet_name)

    # 从SQLite文件中读取数据
    def readFronSqllite(self, exectCmd):
        conn = sqlite3.connect(self.dest_file)  # 该 API 打开一个到 SQLite 数据库文件 database 的链接，如果数据库成功打开，则返回一个连接对象
        cursor = conn.cursor()  # 该例程创建一个 cursor，将在 Python 数据库编程中用到。
        conn.row_factory = sqlite3.Row  # 可访问列信息
        cursor.execute(exectCmd)  # 该例程执行一个 SQL 语句
        rows = cursor.fetchall()  # 该例程获取查询结果集中所有（剩余）的行，返回一个列表。当没有可用的行时，则返回一个空的列表。
        # print(rows[0][2]) # 选择某一列数据
        return rows

    def output(self, sheet_name):
        if self.drive == "excel":
            self.output_to_excel(sheet_name)
        elif self.drive == "sqlite":
            self.output_to_sqlite(sheet_name)
        elif self.drive == "print":
            self.output_to_console(sheet_name)
        else:
            self.output_to_console(sheet_name)
            # self.output_to_excel(sheet_name)
            # self.output_to_sqlite(sheet_name)


def sqlite_to_excel(source_file, sheet_name, dest_file=None):
    import pandas
    if not dest_file:
        dest_file = os.path.splitext(source_file)[0] + ".xlsx"
    con = sqlite3.connect(source_file)
    df = pandas.read_sql('''SELECT * FROM {0}'''.format(sheet_name), con)
    Output(df, dest_file).output_to_excel(sheet_name)


def excel_to_sqlite(source_file, sheet_name, dest_file=None):
    import pandas
    if not dest_file:
        dest_file = os.path.splitext(source_file)[0] + ".xlsx"
    df = pandas.read_excel(source_file, sheet_name=sheet_name)
    Output(df, dest_file).output_to_sqlite(sheet_name)


def main():
    sqlite_to_excel("所有基金数据.db", "自选基金概况")
    excel_to_sqlite("所有基金数据.xlsx", "自选基金概况")


if __name__ == "__main__":
    main()
