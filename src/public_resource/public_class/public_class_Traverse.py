#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 遍历目录
import collections
import glob
import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
import codecs
import os
from public_class.public_class_Data import Data
from public_class.public_class_FileInfo import FileInfo


class Traverse(Data):
    # 初始化函数，类似于构造函数
    def __init__(self, source_dir):
        super(Traverse, self).__init__()
        self.source_dir = source_dir

    def traverse(self, list_except_dir, list_except_file, list_except_key_word):
        list_file = []
        for root, dirs, files in os.walk(self.source_dir):
            for ignore in list_except_dir:
                if ignore in dirs:
                    dirs.remove(ignore)
                    logging.debug("[1]过滤掉的目录:%s" % ignore)

            for ignore in list_except_file:
                if ignore in files:
                    files.remove(ignore)
                    source_file = os.path.join(root, ignore).encode('utf-8').decode('utf-8')
                    logging.debug("[2]过滤掉的文件:%s" % source_file)

            for filename in files:
                source_file = os.path.join(root, filename).encode('utf-8').decode('utf-8')
                # if Data().is_exist_keyword(list_except_dir, root):
                #     # logging.debug("[1]过滤掉的目录:%s" % source_file)
                #     continue
                # if Data().is_exist_keyword(list_except_file, filename):
                #     logging.debug("[2]过滤掉的文件:%s" % source_file)
                #     continue
                # 文件不为空
                if os.path.getsize(source_file):
                    if Data().is_exist_keyword(list_except_key_word, source_file):
                        # logging.debug("[4]过滤掉敏感词的文件:%s" % source_file)
                        continue
                    else:
                        list_file.append(source_file)
                else:
                    # logging.debug("[3]过滤掉的空文件:%s" % source_file)
                    continue
        return list_file

    # 遍历source_dir下所有子文件,并过滤关键文件
    def filter_file(self, key_filename=None):
        list_file = []
        for root, dirs, files in os.walk(self.source_dir):
            for filename in files:
                source_file = os.path.join(root, filename).encode('utf-8').decode('utf-8')
                # 文件不为空
                if os.path.getsize(source_file):
                    if ((key_filename != None and key_filename == os.path.basename(source_file))
                            or key_filename == None):
                        list_file.append(source_file)
                else:
                    logging.debug("空文件:%s" % source_file)
                    continue
        return list_file

    # 遍历source_dir下所有子文件,并过滤文件后缀
    def filter_file_by_suffix(self, suffix=None):
        list_file = []
        for root, dirs, files in os.walk(self.source_dir):
            for filename in files:
                name, suf = os.path.splitext(filename)
                if (suffix != None and suf == suffix) or suffix == None:
                    list_file.append(os.path.join(root, filename).encode('utf-8').decode('utf-8'))
        return list_file

    def filter_file_by_suffix_2(self, suffix):
        list_file = []
        for file in glob.iglob(f"{self.source_dir}/**/*{suffix}", recursive=True):
            list_file.append(file)
        return list_file

    # 统计source_dir下目录和文件的个数
    def get_the_number_of_current_dir(self, start_path=None):
        if None == start_path:
            start_path = self.source_dir
        dir_count = 0
        file_count = 0
        file_list = os.listdir(start_path)
        for item in file_list:
            file_dir = os.path.join(start_path, item)
            if os.path.isdir(file_dir):
                dir_count += 1
            else:
                file_count += 1
        return (dir_count, file_count)

    # 遍历source_dir下所有子目录
    def get_subdir(self, start_path=None, dict_dir=None, tree_level=0):
        if None == start_path:
            start_path = self.source_dir
        if None == dict_dir:
            dict_dir = collections.defaultdict(list)

        tree_level += 1  # 第几层子目录
        file_list = os.listdir(start_path)
        for item in file_list:
            file_dir = os.path.join(start_path, item)
            if os.path.isdir(file_dir):
                dict_dir[tree_level].append(file_dir)
                dict_dir = self.get_subdir(file_dir, dict_dir, tree_level)
            else:
                continue
        # print(dict_dir)
        return dict_dir

    # 查找当前目录下后缀为txt的文件哪个含有“Python”字符
    def search_dir(self, suffix, key_word):
        list_2D = []
        for (dirpath, dirs, files) in os.walk(self.source_dir):
            for file in files:
                if os.path.splitext(file)[1] == suffix:
                    filepath = os.path.join(dirpath, file)
                    with codecs.open(filepath, 'r', FileInfo(filepath).get_file_encoding()) as fr:
                        linenum = 0
                        for line in fr:
                            linenum += 1
                            if key_word in line:
                                # print(filepath, linenum)
                                list_2D.append([filepath, linenum])

        return list_2D

    # 三个参数：分别返回1.父目录 2.所有文件夹名字（不含路径） 3.所有文件名字
    def traverse_1(self):
        list_dir = []
        list_file = []
        for root, dirs, files in os.walk(self.source_dir):
            # 输出文件夹信息
            for dirname in dirs:
                source_dir = os.path.join(root, dirname).encode('utf-8').decode('utf-8')
                # print("root is:" + root)
                # print("\t dirname is:" + dirname)
                # print(source_dir)
                list_dir.append(source_dir)

            # 输出文件信息
            for filename in files:
                source_file = os.path.join(root, filename).encode('utf-8').decode('utf-8')
                # 文件不为空
                if os.path.getsize(source_file):
                    list_file.append(source_file)
                    logging.debug("待处理的文件:%s" % source_file)
                else:
                    logging.debug("空文件:%s" % source_file)
                    continue
        return list_dir, list_file

    # 方法二：os.walk
    def traverse_2(self):
        list_file = []
        for root, dirs, files in os.walk(self.source_dir):
            logging.debug(root)
            logging.debug("\t" * 2 + dirs)
            logging.debug("\t" * 4 + files)
            for filename in files:
                source_file = os.path.join(root, filename).encode('utf-8').decode('utf-8')
                # 文件不为空
                if os.path.getsize(source_file):
                    list_file.append(source_file)
                else:
                    logging.debug("空文件:%s" % source_file)
                    continue
        return list_file

    # 方法三：os.listdir——遍历filepath下所有文件，包括子目录
    def traverse_3(self, start_path, list_file=None):
        if None == list_file:
            list_file = []
        file_list = os.listdir(start_path)
        for item in file_list:
            file_dir = os.path.join(start_path, item)
            if os.path.isdir(file_dir):
                self.traverse_3(file_dir, list_file)
            else:
                source_file = file_dir.encode('utf-8').decode('utf-8')
                # 文件不为空
                if os.path.getsize(source_file):
                    list_file.append(source_file)
                else:
                    logging.debug("空文件:%s" % source_file)
                    continue
        return list_file


def main():
    source_dir = r'C:\Intel'
    obj = Traverse(source_dir)
    obj.search_dir('.txt', 'python')

    list_1D = None
    choice = str(input("Enter a positive integer to choice: "))
    if choice == "1":
        list_dir, list_1D = obj.traverse_1()
    elif choice == "2":
        list_1D = obj.traverse_2()
    elif choice == "3":
        list_1D = obj.traverse_3(source_dir)
    elif choice == "4":
        list_1D = obj.get_subdir()
    elif choice == "5":
        list_1D = obj.filter_file_by_suffix_2('.log')
    else:
        print("未知命令", choice)

    print(list_1D)


if __name__ == "__main__":
    main()
