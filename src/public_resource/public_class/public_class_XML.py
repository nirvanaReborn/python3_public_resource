#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/alapha/p/7460611.html
# https://blog.csdn.net/yuanchao99/article/details/42010011
import os
import codecs
import urllib.parse
import re
from public_class.public_class_FileInfo import FileInfo
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET


class XML(FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name):
        super(XML, self).__init__(file_name)
        self.source_file = os.path.abspath(file_name)

    def get_root(self):
        encoding = FileInfo(self.source_file).get_file_encoding()
        file_content = open(self.source_file, 'r', encoding=encoding, errors='ignore').read()
        # try:
        #     # 将字符串进行解码编码
        #     _str = urllib.parse.unquote(file_content)
        #     _str = _str.decode('gb2312').encode('utf-8')
        #     print(_str[0:100])
        # except Exception:
        #     _str = ""
        #     print('error')
        #
        # # 修改xml文件的编码方式
        # file_content = re.sub('GBK', 'utf-8', _str)

        root = ET.fromstring(file_content)
        return root

    def print_node(self, node):
        if node.tag is not None:
            print(node.tag, ':', node.text)  # 当前节点名

            if node.attrib:
                print(node.attrib)  # 当前节点属性(数据字典)

        if node.getchildren():
            for child in node.getchildren():  # 子节点
                self.print_node(child)

    def print_all_node(self):
        self.print_node(self.get_root())


def main():
    # source_file = r"D:\SVN\PBOX\trunk\Sources\workspace\MS\auto_sett_step_config.xml"
    source_file = r"D:\SVN\PBOX\branches\BL2018SP0Pack4\Sources\workspace\中信建投\switch_config_kmap.xml"
    XML(source_file).print_all_node()


if __name__ == "__main__":
    main()
