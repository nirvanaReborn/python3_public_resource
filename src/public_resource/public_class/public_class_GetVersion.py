#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 获取版本信息(Vm.n.j.0)

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
import win32api
import re
import os
import chardet
from public_class.public_class_FileInfo import FileInfo
from public_class.public_class_Excel import Excel
from public_constant.public_resource_constant import GLOBAL_WORK_DIR


class GetVersion(FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name):
        super(GetVersion, self).__init__(file_name)
        self.file_name = os.path.abspath(file_name)
        self.suffix = self.get_file_suffix()

    def get_dll_version(self):
        try:
            info = win32api.GetFileVersionInfo(self.file_name, os.sep)
            ms = info['FileVersionMS']
            ls = info['FileVersionLS']
            version = 'V%d.%d.%d.%d' % (win32api.HIWORD(ms), win32api.LOWORD(ms), win32api.HIWORD(ls), win32api.LOWORD(ls))
            list_1D = [os.path.basename(self.file_name), self.suffix, version, self.file_name]
        except:
            list_1D = [os.path.basename(self.file_name), self.suffix, "Vm.n.j.0", self.file_name]

        return list_1D

    def get_so_version(self):
        list_1D = []
        pattern = re.compile(r'V\d+\.\d+\.\d+\.\d+')
        with open(self.file_name, "rb") as fr:
            for fileLine in fr.readlines():
                list_re = pattern.findall(fileLine.decode('gb2312', 'ignore'))
                if list_re:
                    list_1D = [os.path.basename(self.file_name), self.suffix, list_re[0], self.file_name]
                    break
                else:
                    list_1D = [os.path.basename(self.file_name), self.suffix, "Vm.n.j.0", self.file_name]

        return list_1D

    def get_excel_version(self):
        list_sheet = []
        try:
            list_sheet = Excel(self.file_name).read_excel_xlsx_by_sheet('version')
        except:
            # print(self.file_name) # 'Worksheet version does not exist.'
            pass

        if list_sheet:
            list_1D = [os.path.basename(self.file_name), self.suffix, list_sheet[1][0], self.file_name]
        else:
            list_1D = [os.path.basename(self.file_name), self.suffix, "Vm.n.j.0", self.file_name]
        return list_1D

    def get_txt_version(self):
        list_1D = []
        pattern = re.compile(r'V\d+\.\d+\.\d+\.\d+')
        with open(self.file_name, "rb") as fd:
            encoding = chardet.detect(fd.read(1024))["encoding"]

        with open(self.file_name, 'r', encoding=encoding, errors='ignore') as fr:
            list_file_content = fr.readlines()
            for i, fileLine in enumerate(list_file_content):
                if (fileLine.startswith('--修改版本')
                        or fileLine.startswith('-- 修改版本')
                        or fileLine.lstrip('\t').lstrip().startswith('修改说明')
                        or fileLine.lstrip('\t').lstrip().startswith('修改版本')):
                    list_re = pattern.findall(list_file_content[i + 1])
                    if list_re:
                        list_1D = [os.path.basename(self.file_name), self.suffix, list_re[0], self.file_name]
                    else:
                        list_1D = [os.path.basename(self.file_name), self.suffix, "Vm.n.j.0", self.file_name]

                    break
                else:
                    list_1D = [os.path.basename(self.file_name), self.suffix, "Vm.n.j.0", self.file_name]
        return list_1D

    # 插件 后缀 版本 路径
    def get_version(self):
        if self.suffix in (".dll", ".exe"):
            return self.get_dll_version()
        elif self.suffix == ".so":
            return self.get_so_version()
        elif self.suffix in (".xls", ".xlsx"):
            return self.get_excel_version()
        elif self.suffix in (".xml", ".sql"):
            return self.get_txt_version()
        else:
            print('不支持版本获取', self.file_name)
            return [os.path.basename(self.file_name), self.suffix, "Vm.n.j.0", self.file_name]

    # # 获取多个目录下面动态库的版本信息
    # def create_file(self, list_dir, dest_file=None):
    #     dict_sheet = {}
    #     dest_file = os.path.join(GLOBAL_WORK_DIR, r"插件版本.xlsx")
    #     for i, source_dir in enumerate(list_dir):
    #         list_2D = traverse_3(source_dir)
    #         list_2D.insert(0, "插件 版本 路径".split())
    #         dict_sheet[str(i)] = list_2D
    #     Excel(dest_file).create_excel(dict_sheet)
    #     return dest_file


class CompareVersion():
    # 初始化函数，类似于构造函数
    def __init__(self):
        super(CompareVersion, self).__init__()
        # self.list_version = []
        # for item in list_version:
        #     # 默认保护(Vm.n.j.0)
        #     if item == "":
        #         item = "Vm.n.j.0"
        #     self.list_version.append(str(item).strip())

    def init_version(self, version_1, version_2):
        version_1 = str(version_1).strip()
        version_2 = str(version_2).strip()
        if version_1 == "":
            version_1 = "Vm.n.j.0"
        if version_2 == "":
            version_2 = "Vm.n.j.0"

        list_version_1 = str(version_1).replace("V", "").strip().split(".")
        list_version_2 = str(version_2).replace("V", "").strip().split(".")
        return list_version_1, list_version_2

    def is_same_version(self, version_1, version_2):
        list_version_1, list_version_2 = self.init_version(version_1, version_2)
        if len(list_version_1) == len(list_version_2):
            for i, item in enumerate(list_version_1):
                if list_version_1[i] == list_version_2[i]:
                    if i == len(list_version_1) - 1:
                        return True
                    else:
                        continue
                else:
                    return False
        else:
            print("两个版本位数不同：", version_1, version_2)
            return False

    # 版本比较并返回最高版本(Vm.n.j.0)
    def get_new_version(self, version_1, version_2):
        list_version_1, list_version_2 = self.init_version(version_1, version_2)
        if len(list_version_1) == len(list_version_2):
            if str(list_version_1[0]).isdigit():
                if str(list_version_2[0]).isdigit():
                    for i, item in enumerate(list_version_1):
                        if int(list_version_1[i]) > int(list_version_2[i]):
                            return version_1
                        elif int(list_version_1[i]) < int(list_version_2[i]):
                            return version_2
                        else:
                            if i == len(list_version_1) - 1:
                                return version_1
                            else:
                                continue
                else:
                    return version_1
            else:
                if str(list_version_2[0]).isdigit():
                    return version_2
                else:
                    return version_1
        else:
            print("两个版本位数不同：", version_1, version_2)
            return version_1

    def get_max_version(self, list_version):
        new_version = "V0.0.0.0"
        for i in range(len(list_version)):
            new_version = self.get_new_version(new_version, list_version[i])

        if new_version == "V0.0.0.0":
            new_version = "Vm.n.j.0"
        return new_version

    def version_sort(self, list_version):
        temp = ""
        for i in range(len(list_version)):
            for j in range(len(list_version) - 1):
                if list_version[j] == self.get_new_version(list_version[j], list_version[j+1]):
                    temp = list_version[j+1]
                    list_version[j + 1] = list_version[j]
                    list_version[j] = temp


def main():
    print(CompareVersion().get_new_version("V8.19.2.1", "V8.19.2.3"))
    print(CompareVersion().get_new_version("Vm.n.j.0", "V8.19.2.3"))
    print(sorted(["V8.19.3.1", "V8.19.2.3"]))


if __name__ == "__main__":
    main()
