#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import abc
# import pprint


class Config(metaclass=abc.ABCMeta):  # 只能被继承，不能实例化，实例化会报错
    def __init__(self, file_name):
        self.source_file = os.path.abspath(file_name)
        self.config = None
        try:
            if os.path.exists(self.source_file):
                self.parse_file()
            else:
                print("配置文件不存在:", self.source_file)
        except Exception as e:
            print("配置文件解析失败:", self.source_file, e)

    @abc.abstractmethod  # 加完这个方法子类必须重写这个方法，否则报错
    def parse_file(self):
        pass

    @abc.abstractmethod
    def get_str_config(self, section, option, default_value=""):
        pass

    def get_int_config(self, section, option, default_value=0):
        return int(self.get_str_config(section, option, str(default_value)))

    def get_float_config(self, section, option, default_value=0.0):
        return float(self.get_str_config(section, option, str(default_value)))

    def get_list_config(self, section, option, default_value="", separator=";"):
        result = self.get_str_config(section, option, str(default_value))
        if isinstance(result, list):
            return result
        elif isinstance(result, str):
            return result.split(separator)

        return default_value

    def get_dict_config(self, section, option, default_value=""):
        result = self.get_str_config(section, option, str(default_value))
        if isinstance(result, dict):
            return result
        elif isinstance(result, str):
            result = eval(result)
            if isinstance(result, dict):
                return result

        return default_value


class ConfigFactory:
    __obj = None  # 类属性
    __init_flag = True

    def __new__(cls, *args, **kwargs):
        if cls.__obj is None:
            cls.__obj = object.__new__(cls)
        return cls.__obj

    def __init__(self):
        if ConfigFactory.__init_flag:
            ConfigFactory.__init_flag = False

    @staticmethod
    def create(source_file):
        source_file = os.path.abspath(source_file)
        file_suffix = os.path.splitext(source_file)[1]
        if file_suffix == ".ini" or file_suffix == ".conf":
            return ConfigIni(source_file)
        elif file_suffix == ".json":
            return ConfigJson(source_file)
        elif file_suffix == '.xml':
            return ConfigXml(source_file)
        elif file_suffix == '.yaml':
            return ConfigYaml(source_file)
        else:
            print("未知格式，无法解析:", source_file)
            exit(0)


class ConfigIni(Config):
    def parse_file(self):
        import configparser
        self.config = configparser.ConfigParser()
        self.config.read(self.source_file, 'gbk')

    def get_str_config(self, section, option, default_value=""):
        if self.config.has_option(section, option):
            return self.config.get(section, option)
        else:
            return default_value


class ConfigJson(Config):
    def parse_file(self):
        import json
        try:
            # 设置以utf-8解码模式读取文件，encoding参数必须设置，
            # 否则默认以gbk模式读取文件，当文件中包含中文时，会报错
            with open(self.source_file, 'r', encoding='utf-8') as fd:
                self.config = json.load(fd)
                # print(self.config)
        except Exception as e:
            print("配置文件解析失败:", self.source_file, e)
            self.config = {}

    def get_str_config(self, section, option, default_value=""):
        return self.config.get(section, {}).get(option, default_value)


class ConfigYaml(Config):
    def parse_file(self):
        import yaml
        try:
            # 设置以utf-8解码模式读取文件，encoding参数必须设置，
            # 否则默认以gbk模式读取文件，当文件中包含中文时，会报错
            with open(self.source_file, 'r', encoding='gb2312') as fd:
                self.config = yaml.load(fd, Loader=yaml.FullLoader)
                # pprint.pprint(self.config)
        except Exception as e:
            print("配置文件解析失败:", self.source_file, e)
            self.config = {}

    def get_str_config(self, section, option, default_value=""):
        return self.config.get(section, {}).get(option, default_value)


class ConfigXml(Config):
    def parse_file(self):
        try:
            import xml.etree.cElementTree as ET
        except ImportError:
            import xml.etree.ElementTree as ET

        try:
            self.config = ET.parse(self.source_file)
            # root = self.config.getroot()
        except Exception as e:
            print("配置文件解析失败:", self.source_file, e)
            self.config = None

    def get_str_config(self, section, option, default_value=""):
        if self.config:
            list_node = self.config.findall(section)
            # print(list_node)
            for node in list_node:
                # print(node.attrib)
                # print(node.tag, ':', node.text)
                for child in node.getchildren():
                    if child.tag == option:
                        return child.text

        return default_value


def test_1():
    source_file = r"../../../../python3_data_analysis/src/data_analysis/file_analysis/ini/config.ini"
    factory = ConfigFactory()
    config = factory.create(source_file)
    print(config.get_str_config("global", "Artist"))
    print(config.get_int_config("global", "db_port"))


def test_2():
    source_file = r"../../../test/config.yaml"
    factory = ConfigFactory()
    config = factory.create(source_file)
    print(config.get_str_config("my", "age"))
    print(config.get_list_config("multi", "list"))
    print(config.get_dict_config("multi", "dict"))


def main():
    # test_1()
    test_2()


if __name__ == "__main__":
    main()
