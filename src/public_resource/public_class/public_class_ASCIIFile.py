#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 处理纯文本文件

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    # format='%(message)s'
                    )
import codecs
import os
import colorama
from public_class.public_class_FileInfo import FileInfo
from public_constant.public_resource_constant import GLOBAL_WORK_DIR, GLOBAL_TXT_FILE


class ASCIIFile(FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name=GLOBAL_TXT_FILE):
        super(ASCIIFile, self).__init__(file_name)
        self.source_file = os.path.abspath(file_name)

    def create_ASCII_file(self, file_content, encoding="gbk", append=False, cover=True):
        if cover:  # 覆盖
            if os.path.exists(self.source_file):
                if append:
                    with codecs.open(self.source_file, 'a', encoding) as fw:
                        fw.write(file_content)
                        logging.debug(colorama.Fore.GREEN + "追加超文本文件:" + self.source_file + colorama.Style.RESET_ALL)
                else:
                    with codecs.open(self.source_file, 'w', encoding) as fw:
                        fw.write(file_content)
                        logging.debug(colorama.Fore.GREEN + "生成超文本文件:" + self.source_file + colorama.Style.RESET_ALL)
            else:
                with codecs.open(self.source_file, 'w', encoding) as fw:
                    fw.write(file_content)
                    logging.debug(colorama.Fore.GREEN + "生成超文本文件:" + self.source_file + colorama.Style.RESET_ALL)
        else:  # 不覆盖
            if os.path.exists(self.source_file):
                logging.debug(colorama.Fore.BLUE + "%s文件已存在" + self.source_file + colorama.Style.RESET_ALL)
            else:
                with codecs.open(self.source_file, 'w', encoding) as fw:
                    fw.write(file_content)
                    logging.debug(colorama.Fore.GREEN + "生成超文本文件:" + self.source_file + colorama.Style.RESET_ALL)
        return self.source_file

    def create_list1D_to_txt(self, file_content, encoding="gbk", append=False):
        if file_content:
            if append:
                fw = codecs.open(self.source_file, 'a', encoding, errors='ignore')
            else:
                fw = codecs.open(self.source_file, 'w', encoding, errors='ignore')
            fw.write('\n'.join([str(item) for item in file_content]))
            fw.close()
            logging.debug(colorama.Fore.GREEN + "生成超文本文件:" + self.source_file + colorama.Style.RESET_ALL)
        else:
            logging.debug("%s文件内容为空：%s" % (self.source_file, file_content))
        return self.source_file

    def create_list2D_to_txt(self, file_content, encoding="gbk", append=False, space_flag=";"):
        if append:
            fw = codecs.open(self.source_file, 'a', encoding, errors='ignore')
        else:
            fw = codecs.open(self.source_file, 'w', encoding, errors='ignore')

        for fileLine in file_content:
            fw.write(space_flag.join([str(item) for item in fileLine]) + '\n')

        fw.close()
        logging.debug(colorama.Fore.GREEN + "生成超文本文件:" + self.source_file + colorama.Style.RESET_ALL)
        return self.source_file

    def create_to_text(self, file_content, encoding="gbk", append=False, space_flag=";"):
        def get_dimensionality(lst):
            '''获取列表维度'''
            if not (isinstance(lst, list) or isinstance(lst, tuple)):
                return 0
            return 1 + get_dimensionality(lst[0])

        if get_dimensionality(file_content) == 1:
            self.create_list1D_to_txt(file_content, encoding, append)
        else:
            self.create_list2D_to_txt(file_content, encoding, append, space_flag)

    def read_file_list(self, encoding='gbk'):
        try:
            with codecs.open(self.source_file, 'r', encoding) as fr:
                # readlines会把内容以列表的形式输出
                list_file_content = fr.readlines()
            return list_file_content
        except:
            try:
                encoding = self.get_file_encoding()
                with codecs.open(self.source_file, 'r', encoding) as fr:
                    list_file_content = fr.readlines()
                return list_file_content
            except Exception as e:
                print(colorama.Fore.RED + self.source_file + encoding + str(e) + colorama.Style.RESET_ALL)

    def read_file_str(self, encoding='gbk'):
        try:
            with codecs.open(self.source_file, 'r', encoding) as fr:
                # read是逐字符地读取,read可以指定参数，设定需要读取多少字符。
                file_content = fr.read()
            return file_content
        except:
            try:
                encoding = self.get_file_encoding()
                with codecs.open(self.source_file, 'r', encoding) as fr:
                    file_content = fr.read()
                return file_content
            except Exception as e:
                print(colorama.Fore.RED + self.source_file + encoding + str(e) + colorama.Style.RESET_ALL)

    def is_text_file(self, blocksize=512):
        import mimetypes
        import chardet
        mime_type, _ = mimetypes.guess_type(self.source_file)
        if mime_type and mime_type.startswith('text'):
            return True

        try:
            with open(self.source_file, 'rb') as file:
                # 读取部分数据以判断编码
                rawdata = file.read(1024)  # 读取前1KB内容
                result = chardet.detect(rawdata)

                # 如果检测到的是文本编码（如utf-8或ascii），则判断为文本文件
                if result['encoding']:
                    return True

                # 读取一块数据
                file.seek(0)
                block = file.read(blocksize)

                # 判断是否为文本文件
                if b'\0' in block:
                    return False
                else:
                    text_characters = bytearray(range(32, 127)) + b'\n\r\t\f\b'
                    non_text = block.translate(None, text_characters)
                    return len(non_text) == 0
        except:
            return False

    # def encoding_convert(self, new_file, toEncode):
    #     fromEncode = FileInfo(self.source_file).get_file_encoding()
    #     f = codecs.open(new_file, 'wb', toEncode)
    #     f.write(self.read_file_str().decode(fromEncode).encode(toEncode))
    #     f.close()


def main():
    file = ASCIIFile(r"../../public_constant/public_resource_constant.py")
    print(file.read_file_list())
    # login_file_xml = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xml")
    # dest_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.txt")
    # ASCIIFile(login_file_xml).encoding_convert(dest_file, "utf-8")


if __name__ == "__main__":
    main()
