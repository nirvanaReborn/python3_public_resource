#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 处理word文档

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
# pip install python-docx
import docx  # 只能读取docx格式文件
import os
import win32com.client
from public_class.public_class_FileInfo import FileInfo
from public_class.public_class_Excel import Excel
from public_constant.public_resource_constant import GLOBAL_DOCX_FILE


# --------------------------------------------------------------------------
# https://www.cnblogs.com/Jacklovely/p/5750173.html
# 只对windows平台有效
# 既能处理docx格式文档，也能处理doc格式文档
class WordApp:
    '''
   Some convenience methods for Excel documents accessed
   through COM.一些方便的方法为Excel文档接入通过COM
    '''

    def __init__(self, visible=False):
        self.wdApp = win32com.client.Dispatch('Word.Application')
        self.wdApp.Visible = visible  # 后台运行,默认不显示
        self.wdApp.DisplayAlerts = False  # 不警告

    def new(self, filename=None):
        '''
        Create a new Word document. If 'filename' specified,
        use the file as a template.创建一个新的word文件，
        '''
        if filename:
            return self.wdApp.Documents.Add(filename)
        else:
            return self.wdApp.Documents.Add()

    def open(self, filename):
        '''
        Open an existing Word document for editing.
        打开一个已经存在的word进行编辑
        '''
        if os.path.exists(filename):
            return self.wdApp.Documents.Open(filename)
        else:
            logging.error('文件不存在: {}'.format(filename))

    def visible(self, visible=True):
        self.wdApp.Visible = visible

    def find(self, text, MatchWildcards=False):
        '''
        Find the string
        查找字符串
        '''
        find = self.wdApp.Selection.Find
        find.ClearFormatting()
        find.Execute(text, False, False, MatchWildcards, False, False, True, 0)
        return self.wdApp.Selection.Text

    def replaceAll(self, oldStr, newStr):
        '''
        Find the oldStr and replace with the newStr.
        替换所有字符串
        '''
        find = self.wdApp.Selection.Find
        find.ClearFormatting()
        find.Replacement.ClearFormatting()
        find.Execute(oldStr, False, False, False, False, False, True, 1, True, newStr, 2)

    def updateToc(self):
        for tocitem in self.wdApp.ActiveDocument.TablesOfContents:
            tocitem.Update()

    def save(self):
        '''
        Save the active document
        '''
        self.wdApp.ActiveDocument.Save()

    def saveAs(self, filename, delete_existing=True):
        '''
        Save the active document as a different filename.
        If 'delete_existing' is specified and the file already
        exists, it will be deleted before saving.
        '''
        if delete_existing and os.path.exists(filename):
            os.remove(filename)
            self.wdApp.ActiveDocument.SaveAs(FileName=filename)

    def close(self):
        '''
        Close the active workbook.
        '''
        self.wdApp.ActiveDocument.Close()

    def quit(self):
        '''
        Quit Word
        '''
        return self.wdApp.Quit()


# --------------------------------------------------------------------------
# 使用docx扩展包
# 优点:不依赖操作系统,跨平台
# 缺点:只能处理docx格式文档
# pip install python-docx
# 参考文档: https://python-docx.readthedocs.io/en/latest/index.html
class Word(FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name=GLOBAL_DOCX_FILE):
        super(Word, self).__init__(file_name)
        self.source_file = os.path.abspath(file_name)

    def get_text(self):
        list_content = []
        doc = docx.Document(self.source_file)
        # print("段落数:" + str(len(doc.paragraphs)))

        for para in doc.paragraphs:
            list_content.append(' ' + str(para.text).strip())  # 让每一段缩进

        # 在段落之间增加空行
        return '\n'.join(list_content)

    def get_tble(self):
        dict_table = {}
        doc = docx.Document(self.source_file)
        for i, table in enumerate(doc.tables):  # 遍历所有表格
            list_2D = []
            # print("----------table--------------")
            for row in table.rows:  # 遍历每个表格的所有行
                # print("----------row--------------")
                # row_str = '\t'.join([cell.text for cell in row.cells])  # 一行数据
                # print(row_str)

                list_1D = []
                for cell in row.cells:  # 遍历每行的单元格
                    list_1D.append(str(cell.text))
                    # print(cell.text)

                if list_1D:
                    list_2D.append(list_1D)

            if list_2D:
                dict_table[i] = list_2D
        return dict_table

    def get_tble_to_excel(self, dest_file):
        dict_table = self.get_tble()
        Excel(dest_file).create_excel(dict_table)

    def set_table(self, dict_sheet):
        doc = docx.Document()
        if dict_sheet != None and len(dict_sheet) > 0:
            for key, value in dict_sheet.items():
                if value != None and len(value) > 0:
                    # 创建带边框的表格
                    table = doc.add_table(rows=len(value), cols=len(value[0]), style='Table Grid')

                    for i, fileLine in enumerate(value):
                        if len(str(fileLine)) > 0:
                            cells = table.rows[i].cells

                            for j, item in enumerate(fileLine):
                                cells[j].text = fileLine[j]

            print("生成word文档:", self.source_file)
            doc.save(self.source_file)
        else:
            logging.error("数据为空")


# --------------------------------------------------------------------------

def main():
    pass


if __name__ == "__main__":
    main()
