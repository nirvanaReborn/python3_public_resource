#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 获取登录信息

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
import os
import pprint

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
from public_class.public_class_FileInfo import FileInfo
from public_class.public_class_Excel import Excel
from public_constant.public_resource_constant import GLOBAL_WORK_DIR


class GetConfigInfo(FileInfo):
    # 初始化函数，类似于构造函数
    def __init__(self, file_name, filter_tag=r"登录信息"):
        super(GetConfigInfo, self).__init__(file_name)
        self.source_file = os.path.abspath(file_name)
        self.filter_tag = filter_tag
        self.list_unique_index = []

    def analysis_excel(self) -> list:
        list_config = []
        list_sheet = Excel(self.source_file).read_excel_by_sheet(self.filter_tag)
        for i, fileLine in enumerate(list_sheet):
            # print(fileLine)
            if i == 0:
                continue
            dict_row = {}
            for j, cell in enumerate(fileLine):
                dict_row[list_sheet[0][j]] = str(fileLine[j])

            if dict_row:
                list_config.append(dict_row)

        return list_config

    def analysis_xml(self) -> list:
        list_config = []
        tree = ET.parse(self.source_file)
        root = tree.getroot()
        for child in root:  # 第二层节点
            if child.tag == self.filter_tag:
                if "unique_index" in child.keys():
                    unique_index = child.get("unique_index", "")
                    if unique_index:
                        self.list_unique_index = str(unique_index).split(',')

                for subchild in child:  # 第三层节点
                    list_config.append(subchild.attrib)
        return list_config

    def get_list_config(self) -> list:
        filename, suffix = os.path.splitext(self.source_file)
        # print(suffix)
        if ".xml" == suffix:
            return self.analysis_xml()
        elif suffix in (".xlsx", ".xls"):
            return self.analysis_excel()
        elif ".ini" == suffix:
            pass
        else:
            print("未知的文件格式:", self.source_file)
            return []

    def get_dict_config(self) -> dict:
        def get_list_unique_index(dict_info):
            if self.list_unique_index:
                if len(self.list_unique_index) == 1:
                    return str(dict_info[self.list_unique_index[0]])
                else:
                    list_unique_index = []
                    for item in self.list_unique_index:
                        list_unique_index.append(str(dict_info[item]))
                    return tuple(list_unique_index)
            else:
                print("没有指定唯一索引字段列表")
                return None

        dict_config = {}
        list_dict_info = self.get_list_config()
        for dict_info in list_dict_info:
            dict_config[get_list_unique_index(dict_info)] = dict_info

        return dict_config


def main():
    login_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xlsx")
    # Excel(login_file).excel_to_xml()
    # login_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xml")
    pprint.pprint(GetConfigInfo(login_file, r"登录信息").get_list_config())


if __name__ == "__main__":
    main()
