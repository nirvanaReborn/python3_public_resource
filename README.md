# python3_public_resource

## 项目介绍

提供公用类、公用函数、常量等

## 软件版本

*  PyCharm + Anaconda3(V5.1.0)[python3.6.4]
*  [Anaconda版本与Python版本对应关系](https://blog.csdn.net/zyb228/article/details/103251761)
*  [Anaconda历史版本](https://repo.anaconda.com/archive/)


